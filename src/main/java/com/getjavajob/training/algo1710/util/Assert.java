package com.getjavajob.training.algo1710.util;

import java.util.Arrays;

public class Assert {

    public static void fail(String msg) throws AssertionError {
        throw new AssertionError(msg);
    }

    public static void assertEquals(String testName, int [] expected, int [] actual){
        if (Arrays.equals(expected,actual)){
            System.out.println(testName  + " passed");
        } else {
            System.out.println(testName + " failed: expected");
            for (int i: expected) {
                System.out.print(i+ " ");
            }
            System.out.println("actual ");
            for (int i: actual) {
                System.out.print(i+ " ");
            }
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, Object[]expected, Object[]actual) {
        if (Arrays.deepEquals(expected,actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }
}
