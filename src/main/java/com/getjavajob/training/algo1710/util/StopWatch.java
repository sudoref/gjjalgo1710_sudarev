package com.getjavajob.training.algo1710.util;


public class StopWatch {
    private long startTime = 0;
    public long start(){
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public long getElapsedTime(){
        return System.currentTimeMillis() - startTime;
    }
}
