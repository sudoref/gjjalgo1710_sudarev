package com.getjavajob.training.algo1710.util;


public class BinRepresent {
    public static String convertNumber(int a) {
        StringBuilder sb = new StringBuilder();
        sb.append(a & 1);
        while ((a >>= 1) != 0) {
            sb.append(a & 1);
        }
        return sb.reverse().toString();
    }
}
