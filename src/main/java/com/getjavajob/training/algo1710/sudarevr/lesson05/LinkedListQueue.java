package com.getjavajob.training.algo1710.sudarevr.lesson05;

import java.util.LinkedList;
import java.util.List;

public class LinkedListQueue<E> extends AbstractQueue<E> {

    private SinglyLinkedList.Node<E> first;
    private SinglyLinkedList.Node<E> last;
    private int size;

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(E data) {
        SinglyLinkedList.Node<E> node = new SinglyLinkedList.Node<>(data);
        if (isEmpty()) {
            first = node;
        } else {
            last.next = node;
        }
        last = node;
        last.next = null;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public E remove() {
        SinglyLinkedList.Node<E> temp = first;
        if (first.next == null) {
            last = null;
        }
        first = first.next;
        size--;
        return temp.val;
    }

    @Override
    public int size() {
        return size;
    }

    public List asList() {
        List<E> list = new LinkedList<>();
        SinglyLinkedList.Node<E> current = first;
        while (current != null) {
            list.add(current.val);
            current = current.next;
        }
        return list;
    }
}
