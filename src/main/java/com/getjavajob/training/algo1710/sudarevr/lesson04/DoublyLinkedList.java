package com.getjavajob.training.algo1710.sudarevr.lesson04;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<V> extends AbstractList<V> {

    int size;
    private Element<V> first;
    private Element<V> last;

    public DoublyLinkedList() {
    }
    @Override
    public boolean add(V val) {
        Element<V> l = last;
        Element<V> newElement = new Element<>(l, val, null);
        last = newElement;
        if (l == null) {
            first = newElement;
        } else {
            l.next = newElement;
        }
        size++;
        return true;
    }
    @Override
    public void add(int i, V val) {
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("index is negative");
        }
        if (i == size) {
            add(val);
        } else {
            final Element<V> prev = element(i).prev;
            final Element<V> newElement = new Element<>(prev, val, element(i));
            element(i).prev = newElement;
            if (prev == null) {
                first = newElement;
            } else {
                prev.next = newElement;
            }
            size++;
        }
    }

    public V get(int i) {
        checkElementIndex(i);
        return element(i).val;
    }

    public boolean remove(Object val) {
        if (val == null) {
            for (Element<V> x = first; x != null; x = x.next) {
                if (x.val == null) {
                    unlink(x);
                    return true;
                }
            }
        } else {
            for (Element<V> x = first; x != null; x = x.next) {
                if (val.equals(x.val)) {
                    unlink(x);
                    return true;
                }
            }
        }
        return false;
    }

    public V remove(int i) {
        checkElementIndex(i);
        return unlink(element(i));
    }

    private V unlink(Element<V> x) {
        final V element = x.val;
        final Element<V> next = x.next;
        final Element<V> prev = x.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }

        x.val = null;
        size--;
        return element;
    }

    private Element<V> element(int i) {
        if (i < (size >> 1)) {
            Element<V> x = first;
            for (int n = 0; n < i; n++) {
                x = x.next;
            }
            return x;
        } else {
            Element<V> x = last;
            for (int n = size - 1; n > i; n--) {
                x = x.prev;
            }
            return x;
        }
    }

    public int indexOf(Object o) {
        int index = 0;
        if (o == null) {
            for (Element<V> element = first; element != null; element = element.next) {
                if (element.val == null) {
                    return index;
                }
                index++;
            }
        } else {
            for (Element<V> element = first; element != null; element = element.next) {
                if (o.equals(element.val)) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size <= 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Element<V> x = first; x != null; x = x.next) {
            result[i++] = x.val;
        }
        return result;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    public ListIterator<V> listIterator(int index) {
        return new ListIteratorImpl();
    }

    private static class Element<V> {
        Element<V> next;
        Element<V> prev;
        V val;

        Element(Element<V> prev, V element, Element<V> next) {
            this.next = next;
            this.prev = prev;
            this.val = element;
        }
    }

    private class ListIteratorImpl implements ListIterator<V> {
        private Element<V> lastReturned;
        private Element<V> next;
        private int nextIndex;

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public V next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.val;
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        @Override
        public V previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.val;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            Element<V> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned) {
                next = lastNext;
            } else {
                nextIndex--;
            }
            lastReturned = null;
        }

        @Override
        public void set(V v) {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            lastReturned.val = v;
        }

        @Override
        public void add(V val) {
            lastReturned = null;
            if (next == null) {
                DoublyLinkedList.this.add(val);
            } else {
                DoublyLinkedList.this.add(nextIndex, val);
            }
        }
    }
}
