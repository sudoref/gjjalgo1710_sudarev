package com.getjavajob.training.algo1710.sudarevr.lesson05;

import com.getjavajob.training.algo1710.util.Assert;

public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        expressionCalculatorTestCalculate();
        expressionCalculatorTestHasPrecedence();
        expressionCalculatorTestApplyOp();
        expressionCalculatorTestException();
    }

    private static void expressionCalculatorTestCalculate() {
        Assert.assertEquals("expressionCalculatorTestCalculate1", 8, ExpressionCalculator.evaluate("( 2 + 2 ) * 2"));
        Assert.assertEquals("expressionCalculatorTestCalculate2", 6, ExpressionCalculator.evaluate(" 2 + 2 * 2"));
        Assert.assertEquals("expressionCalculatorTestCalculate3", 777, ExpressionCalculator.evaluate(" 120 / 2 * 6 + 417"));
    }

    private static void expressionCalculatorTestHasPrecedence() {
        Assert.assertEquals("expressionCalculatorTestHasPrecedence(true)", true, ExpressionCalculator.hasPrecedence('1', '2'));
        Assert.assertEquals("expressionCalculatorTestHasPrecedence(false)", false, ExpressionCalculator.hasPrecedence('*', '('));
    }

    private static void expressionCalculatorTestApplyOp() {
        Assert.assertEquals("expressionCalculatorTestApplyOp", 6, ExpressionCalculator.applyOp('*', 2, 3));
    }

    private static void expressionCalculatorTestException() {
        try {
            ExpressionCalculator.evaluate(" 2 / 0");
            Assert.fail("expressionCalculatorTestException() failed");
        } catch (Exception e) {
            Assert.assertEquals("expressionCalculatorTestException()", "Cannot divide by zero", e.getMessage());
        }
    }
}
