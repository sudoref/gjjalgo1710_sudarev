package com.getjavajob.training.algo1710.sudarevr.lesson04;

import com.getjavajob.training.algo1710.util.Assert;

public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testSllAdd();
        testSllGet();
        testSllSize();
        testSllRelink();
        testSllReverse();
        testSllAsList();
    }

    private static void testSllAdd() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("testSllAdd");
        Assert.assertEquals("SinglyLinkedListTest.testSllAdd", "testSllAdd", sll.get(0));
    }

    private static void testSllGet() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("testSllGet");
        Assert.assertEquals("SinglyLinkedListTest.testSllGet", "testSllGet", sll.get(0));
    }

    private static void testSllSize() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("testSllSize");
        Assert.assertEquals("SinglyLinkedListTest.testSllSize", 1, sll.size());
    }

    private static void testSllRelink() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("1");
        sll.add("2");
        sll.relink(0, 1);
        Assert.assertEquals("SinglyLinkedListTest.testSllRelink", "21", sll.get(0) + sll.get(1));
    }

    private static void testSllReverse() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("t");
        sll.add("e");
        sll.add("s");
        sll.add("t");
        sll.reverse();
        Assert.assertEquals("SinglyLinkedListTest.testSllReverse", "tset", sll.get(0) + sll.get(1) + sll.get(2) + sll.get(3));
    }

    private static void testSllAsList() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("t");
        sll.add("e");
        sll.add("s");
        sll.add("t");
        Object[] arr = {"t", "e", "s", "t"};
        Assert.assertEquals("SinglyLinkedListTest.testSllAsList", arr, sll.asList().toArray());
    }

}
