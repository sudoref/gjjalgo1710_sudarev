package com.getjavajob.training.algo1710.sudarevr.lesson03;

import com.getjavajob.training.algo1710.util.StopWatch;

import java.util.ArrayList;

public class DynamicArrayPerformanceTest {
    public static void main(String[] args) throws Exception {

        ArrayList al = new ArrayList();
        DynamicArray da = new DynamicArray();
        StopWatch sw = new StopWatch();

        System.out.println("-------- Addition to the beginning --------");
        sw.start();
        for (int i = 0; i < 300000; i++) {
            al.add(0, i);
        }
        System.out.println("ArrayList.add(0,i): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = 0; i < 300000; i++) {
            da.add(0, i);
        }
        System.out.println("DynamicArray.add(0,i): " + sw.getElapsedTime() + " ms\n");

        System.out.println("-------- Addition to the end --------");
        sw.start();
        for (int i = 0; i < 10000000; i++) {
            da.add(i);
        }
        System.out.println("ArrayList.add(e): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = 0; i < 10000000; i++) {
            al.add(i);
        }
        System.out.println("DynamicArray.add(e): " + sw.getElapsedTime() + " ms\n");

        System.out.println("-------- Addition to the middle --------");
        sw.start();
        for (int i = 0; i < 500; i++) {
            al.add(i + 500, i);
        }
        System.out.println("ArrayList.add(i+1000,i): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = 0; i < 500; i++) {
            da.add(i + 500, i);
        }
        System.out.println("DynamicArray.add(i+1000,i): " + sw.getElapsedTime() + " ms\n");

        System.out.println("-------- Removing first elements --------");
        sw.start();
        for (int i = 0; i < 700; i++) {
            al.remove(i);
        }
        System.out.println("ArrayList.remove(i): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = 0; i < 700; i++) {
            da.remove(i);
        }
        System.out.println("DynamicArray.remove(i): " + sw.getElapsedTime() + " ms\n");


        System.out.println("-------- Removing middle elements --------");
        sw.start();
        for (int i = 0; i < 700; i++) {
            al.remove(i + 3000);
        }
        System.out.println("ArrayList.remove(i): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = 0; i < 700; i++) {
            da.remove(i + 3000);
        }
        System.out.println("DynamicArray.remove(i): " + sw.getElapsedTime() + " ms\n");

        System.out.println("-------- Removing last element --------");
        sw.start();
        for (int i = al.size() - 1; i >= 0; i--) {
            al.remove(i);
        }
        System.out.println("ArrayList.remove(i): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = da.size() - 1; i >= 0; i--) {
            da.remove(i);
        }
        System.out.println("DynamicArray.remove(i): " + sw.getElapsedTime() + " ms\n");

    }
}
