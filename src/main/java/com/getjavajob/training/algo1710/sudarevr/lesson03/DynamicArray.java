package com.getjavajob.training.algo1710.sudarevr.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;


class DynamicArray<V> extends AbstractList<V> {
    private static final int DEFAULT_VALUES = 10;
    private V[] values;
    private int size;
    private int counter;

    DynamicArray() {
        this.values = (V[]) new Object[DEFAULT_VALUES];
    }

    DynamicArray(int startValue) {
        if (startValue > 0) {
            this.values = (V[]) new Object[startValue];
        } else if (startValue == 0) {
            this.values = (V[]) new Object[0];
        } else {
            throw new IllegalArgumentException("IllegalArgumentException");
        }
    }

    private String outOfBounds(int i) {
        return "out of bound index: " + i + ", size " + size;
    }

    @Override
    public boolean add(V e) {
        checkValuesLength(size + 1);
        values[size++] = e;
        counter++;
        return true;
    }

    @Override
    public void add(int i, V e) {
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("index is negative");
        }
        checkValuesLength(size + 1);
        System.arraycopy(values, i, values, i + 1, size - i);
        values[i] = e;
        counter++;
        size++;
    }

    @Override
    public V set(int i, V e) {
        if (i >= size) {
            throw new IndexOutOfBoundsException(outOfBounds(i));
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException("index is negative");
        }
        Object prevElem = values[i];
        values[i] = e;
        return (V) prevElem;
    }

    @Override
    public V get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException(outOfBounds(index));
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException("index is negative");
        }
        return values[index];
    }

    @Override
    public V remove(int i) {
        if (i >= size) {
            throw new IndexOutOfBoundsException(outOfBounds(i));
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException("index is negative");
        }
        counter++;
        Object oldValue = values[i];
        int moveElement = size - i - 1;
        if (i < size || i < size - 1) {
            System.arraycopy(values, i + 1, values, i, moveElement);
        }
        values[--size] = null;
        return (V) oldValue;
    }

    @Override
    public boolean remove(Object e) {
        counter++;
        for (int i = 0; i < size; i++) {
            if (e.equals(values[i])) {
                int moveElement = size - i - 1;
                if (moveElement > 0) {
                    System.arraycopy(values, i + 1, values, i, moveElement);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int indexOf(Object e) {
        if(e ==null){
            for(int i = 0; i<size;i++){
                if(values[i]==null){
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(values[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    public Object[] toArray() {
        return Arrays.copyOf(values, size);
    }

    private void checkValuesLength(int value) {
        if (value - values.length > 0) {
            grow();
        }
    }

    private void grow() {
        int oldValue = values.length;
        int newValue = oldValue + (oldValue >> 1);
        if (newValue - oldValue < 0) {
            newValue = oldValue;
        }
        values = Arrays.copyOf(values, newValue);
    }


    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }


    public class ListIteratorImpl implements ListIterator<V> {
        private int index = -1;
        private int counter;

        public ListIteratorImpl() {
            this.counter = DynamicArray.this.counter;
        }

        public boolean hasNext() {
            return index != size - 1;
        }

        public V next() {
            checkConcurrentModificationException();
            if (!hasNext()) {
                throw new NoSuchElementException("NoSuchElementException");
            }
            return DynamicArray.this.get(++index);
        }

        public boolean hasPrevious() {
            return index > 0;
        }

        public V previous() {
            checkConcurrentModificationException();
            if (!hasPrevious()) {
                throw new NoSuchElementException("NoSuchElementException");
            }
            return DynamicArray.this.get(--index);
        }

        public int nextIndex() {
            return hasNext() ? index + 1 : size - 1;
        }

        public int previousIndex() {
            return hasPrevious() ? index - 1 : 0;
        }

        public void remove() {
            checkConcurrentModificationException();
            DynamicArray.this.remove(index);
            counter = DynamicArray.this.counter;
        }

        public void set(V element) {
            checkConcurrentModificationException();
            DynamicArray.this.set(index++, element);
        }

        public void add(V element) {
            checkConcurrentModificationException();
            DynamicArray.this.add(++index, element);
            counter = DynamicArray.this.counter;
        }

        private void checkConcurrentModificationException() {
            if (counter != DynamicArray.this.counter) {
                throw new ConcurrentModificationException("ConcurrentModificationException");
            }
        }
    }
}
