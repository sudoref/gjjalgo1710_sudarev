package com.getjavajob.training.algo1710.sudarevr.lesson06;

interface Matrix<V> {

    V get(int i, int j);

    void set(int i, int j, V value);
}
