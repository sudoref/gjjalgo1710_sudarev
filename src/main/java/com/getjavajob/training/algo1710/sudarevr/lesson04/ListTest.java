package com.getjavajob.training.algo1710.sudarevr.lesson04;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {
        testAddAllListIndex();
        testGet();
        testSet();
        testIndexOf();
        testRemoveList();
        testAddList();
        testLastIndexOfList();
        testSubList();
    }

    private static void testAddAllListIndex() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        ArrayList<Integer> al = new ArrayList<>();
        al.add(3);
        al.add(4);
        Assert.assertEquals("testAddAllListIndex()", true, list.addAll(2, al));
    }

    private static void testGet() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        Assert.assertEquals("testGet()", 2, list.get(1));
    }

    private static void testSet() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        Assert.assertEquals("testSet()", 2, list.set(1, 10));
    }

    private static void testIndexOf() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        Assert.assertEquals("testIndexOf()", 1, list.indexOf(2));
    }

    private static void testRemoveList() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        Assert.assertEquals("testRemoveList()", 1, list.remove(0));
    }

    private static void testAddList() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(1, 5);
        Assert.assertEquals("testAddList()", 5, list.get(1));
    }

    private static void testLastIndexOfList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add("test");
        }
        Assert.assertEquals("testLastIndexOfList()", 99, list.lastIndexOf("test"));
    }

    private static void testSubList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add("test");
        }
        String[] arr = {"test", "test", "test", "test", "test"};
        List<String> newList = list.subList(5, 10);
        Assert.assertEquals("testSubList()", arr, newList.toArray());
    }

}
