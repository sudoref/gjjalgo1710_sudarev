package com.getjavajob.training.algo1710.sudarevr.lesson10;

import static com.getjavajob.training.algo1710.sudarevr.lesson10.QuickSort.sort;
import static com.getjavajob.training.algo1710.util.Assert.assertEquals;


public class QuickSortTests {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        Comparable[] arr = {1, 8, 2, 6, 9};
        Comparable[] sortedArr = {1, 2, 6, 8, 9};

        assertEquals("testSort", sortedArr, sort(arr));
    }
}
