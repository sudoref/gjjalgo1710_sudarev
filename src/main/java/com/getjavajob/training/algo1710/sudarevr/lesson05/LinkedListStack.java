package com.getjavajob.training.algo1710.sudarevr.lesson05;

import java.util.List;

public class LinkedListStack<E> implements Stack<E> {

    private SinglyLinkedList<E> sll = new SinglyLinkedList<E>();

    @Override
    public void push(E e) {
        sll.addFirst(e);
    }

    @Override
    public E pop() {
        return sll.removeFirst();
    }

    @Override
    public E peek() {
        return sll.get(sll.size() - 1);
    }

    public boolean empty() {
        return sll.size() == 0;
    }

    public List<E> asList() {
        return sll.asList();
    }
}
