package com.getjavajob.training.algo1710.sudarevr.lesson05;

public interface Transformer<I, O> {
    O transform(I var1);
}
