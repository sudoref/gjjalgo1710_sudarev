package com.getjavajob.training.algo1710.sudarevr.lesson10;

import static com.getjavajob.training.algo1710.sudarevr.lesson10.InsertionSort.sort;
import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class InsertionSortTests {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        int[] arr = {5, 6, 2, 8, 1};
        int[] sortedArr = {1, 2, 5, 6, 8};

        assertEquals("testSort", sortedArr, sort(arr, arr.length - 1));
    }
}
