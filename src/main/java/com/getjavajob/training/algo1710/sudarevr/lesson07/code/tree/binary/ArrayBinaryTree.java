package com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {

    private NodeImpl[] nodeArray = new NodeImpl[100];
    private int size;

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        int index = 2 * ((NodeImpl<E>) p).getIndex() + 1;
        return nodeArray[index];
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        int index = 2 * ((NodeImpl<E>) p).getIndex() + 2;
        return nodeArray[index];
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        int index = 2 * ((NodeImpl<E>) n).getIndex() + 1;
        NodeImpl<E> newNode = new NodeImpl<>();
        newNode.setIndex(index);
        newNode.setElement(e);
        nodeArray[index] = newNode;
        size++;
        return newNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        int index = 2 * ((NodeImpl<E>) n).getIndex() + 2;
        NodeImpl<E> newNode = new NodeImpl<>();
        newNode.setIndex(index);
        newNode.setElement(e);
        nodeArray[index] = newNode;
        size++;
        return newNode;
    }

    @Override
    public Node<E> root() {
        return nodeArray[0];
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int index = ((NodeImpl) n).getIndex();
        if (index == 0) {
            return null;
        } else if (index % 2 == 0) {
            return nodeArray[(index - 2) / 2];
        } else {
            return nodeArray[(index - 1) / 2];
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (e == null) {
            throw new IllegalStateException();
        }
        NodeImpl<E> newNode = new NodeImpl<>();
        newNode.setElement(e);
        newNode.setIndex(0);
        nodeArray[0] = newNode;
        size++;
        return nodeArray[0];
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        if (nodeArray[0] == null) {
            return addRoot(e);
        } else if (nodeArray[2 * ((NodeImpl) n).getIndex() + 1] == null) {
            return addLeft(n, e);
        } else if (nodeArray[2 * ((NodeImpl) n).getIndex() + 1] == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        nodeArray[((NodeImpl<E>) n).getIndex()].setElement(e);
        return n.getElement();
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int index = ((NodeImpl) n).getIndex();
        if (n == root()) {
            throw new IllegalArgumentException("Root can't be deleted unambiguously");
        } else if (childrenNumber(n) > 1) {
            throw new IllegalArgumentException("In a binary tree, a node with two children cannot be deleted unambiguously");
        } else if (right(n) == null) {
            nodeArray[index] = (NodeImpl) left(n);
            return n.getElement();
        } else if (left(n) == null) {
            nodeArray[index] = (NodeImpl) right(n);
            return n.getElement();
        } else if (children(n) == null) {
            nodeArray[index] = null;
            return n.getElement();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        List<E> list = new ArrayList<>();
        for (Node<E> node : nodes()) {
            list.add(node.getElement());
        }
        return list.iterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return inOrder();
    }

    @Override
    public Collection<Node<E>> preOrder() {
        List<Node<E>> list = new ArrayList<>();
        Deque<Node<E>> stack = new LinkedList<>();
        stack.push(root());

        while (!stack.isEmpty()) {
            Node<E> n = stack.pop();
            list.add(n);

            if (right(n) != null) {
                stack.push(right(n));
            }
            if (left(n) != null) {
                stack.push(left(n));
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> inOrder() {
        List<Node<E>> list = new ArrayList<>();
        Stack<Node<E>> stack = new Stack<>();
        Node<E> currentNode = root();
        while (!stack.empty() || currentNode != null) {
            if (currentNode != null) {
                stack.push(currentNode);
                currentNode = left(currentNode);
            } else {
                Node<E> n = stack.pop();
                list.add(n);
                currentNode = right(n);
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        List<Node<E>> list = new ArrayList<>();
        Stack<Node<E>> stack = new Stack<>();
        Node<E> current = root();

        while (true) {
            if (current != null) {
                if (right(current) != null) {
                    stack.push(right(current));
                }
                stack.push(current);
                current = left(current);
                continue;
            }
            if (stack.isEmpty()) {
                break;
            }
            current = stack.pop();

            if (right(current) != null && !stack.isEmpty() && right(current) == stack.peek()) {
                stack.pop();
                stack.push(current);
                current = right(current);
            } else {
                list.add(current);
                current = null;
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        List<Node<E>> list = new ArrayList<>();
        Queue<Node<E>> queue = new LinkedList<>();
        queue.add(root());
        while (!queue.isEmpty()) {
            Node<E> n = queue.poll();
            list.add(n);
            if (left(n) != null) {
                queue.add(left(n));
            }
            if (right(n) != null) {
                queue.add(right(n));
            }
        }
        return list;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (NodeImpl<E> nodes : nodeArray) {
            if (nodes != null) {
                builder.append(nodes.getElement()).append(" ");
            }
        }
        return builder.toString();
    }

    protected static class NodeImpl<E> implements Node<E> {

        private E element;
        private int index;

        public NodeImpl() {
        }

        public NodeImpl(int index, E element) {
            this.index = index;
            this.element = element;
        }

        @Override
        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }
}
