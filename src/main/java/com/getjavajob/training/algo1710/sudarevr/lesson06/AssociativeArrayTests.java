package com.getjavajob.training.algo1710.sudarevr.lesson06;

import com.getjavajob.training.algo1710.util.Assert;

public class AssociativeArrayTests {
    public static void main(String[] args) {
        associativeArrayTestsAdd();
        associativeArrayTestsGet();
        associativeArrayTestsRemove();
        associativeArrayTestsSize();
    }

    private static void associativeArrayTestsAdd() {
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("1", 1);
        aa.add("random", 10);
        aa.add("polygenelubricants", 20);

        Assert.assertEquals("associativeArrayTestsAdd()random", 10, aa.get("random"));
        Assert.assertEquals("associativeArrayTestsAdd()polygenelubricants", 20, aa.get("polygenelubricants"));
        Assert.assertEquals("associativeArrayTestsAdd()newValue", 10, aa.add("random", 100));
    }

    private static void associativeArrayTestsGet() {
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("1", 1);
        aa.add("random", 10);
        aa.add("polygenelubricants", 20);

        Assert.assertEquals("associativeArrayTestsGet()", 20, aa.get("polygenelubricants"));
    }

    private static void associativeArrayTestsRemove() {
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("1", 1);
        aa.add("random", 10);
        aa.add("polygenelubricants", 20);

        Assert.assertEquals("associativeArrayTestsRemove()", 20, aa.remove("polygenelubricants"));
    }

    private static void associativeArrayTestsSize() {
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("1", 1);
        aa.add("random", 10);
        aa.add("polygenelubricants", 20);

        Assert.assertEquals("associativeArrayTestsSize()", 3, aa.size());
    }
}
