package com.getjavajob.training.algo1710.sudarevr.lesson10;

public class InsertionSort {

    public static int[] sort(int[] a, int n) {
        if (n > 0) {
            sort(a, n - 1);
            int x = a[n];
            int j = n - 1;
            while (j >= 0 && a[j] > x) {
                a[j + 1] = a[j];
                j = j - 1;
            }
            a[j + 1] = x;
        }
        return a;
    }
}
