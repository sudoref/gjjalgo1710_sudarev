package com.getjavajob.training.algo1710.sudarevr.lesson01;

public class Task07 {

    public static String arithmeticSolution1(int x, int y) {
        x = x * y;
        y = x / y;
        x = x / y;
        return "x = " + x + ", y = " + y;
    }

    public static String arithmeticSolution2(int x, int y) {
        x = x + y;
        y = x - y;
        x = x - y;
        return "x = " + x + ", y = " + y;
    }

    public static String bitwiseSolution1(int x, int y) {
        x = (x & ~y) | (~x & y);
        y = (y & ~x) | (~y & x);
        x = (x & ~y) | (~x & y);
        return "x = " + x + ", y = " + y;
    }

    public static String bitwiseSolution2(int x, int y) {
        x = x ^ y;
        y = x ^ y;
        x = x ^ y;
        return "x = " + x + ", y = " + y;
    }

}
