package com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class LinkedBinaryTreeTests {
    public static void main(String[] args) {
        testAddRoot();
        testAdd();
        testAddRight();
        testAddLeft();
        testSet();
        testRemove();
        testLeft();
        testRight();
        testRoot();
        testParent();
        testSize();
        testNodes();
        testPreOrder();
        testInOrder();
        testPostOrder();
        testBreadthFirst();
    }

    private static void testAddRoot() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(10);
        assertEquals("LinkedBinaryTreeTests.testAddRoot", 10, lbt.root().getElement());
    }

    private static void testAdd() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.add(node2, 70);

        String s = "10 20 30 40 50 60 70 ";

        assertEquals("LinkedBinaryTreeTests.testAdd", s, lbt.toString());
    }

    private static void testAddRight() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        lbt.addRight(root, 20);
        assertEquals("LinkedBinaryTreeTests.testAddRight", 20, lbt.right(root).getElement());
    }

    private static void testAddLeft() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        lbt.addLeft(root, 20);
        assertEquals("LinkedBinaryTreeTests.testAddLeft", 20, lbt.left(root).getElement());
    }

    private static void testSet() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        lbt.set(root, 20);
        assertEquals("LinkedBinaryTreeTests.testAddLeft", 20, lbt.root().getElement());
    }

    private static void testRemove() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);
        lbt.remove(node5);
        String s = "10 20 30 40 50 70 ";

        assertEquals("LinkedBinaryTreeTests.testRemove", s, lbt.toString());
    }

    private static void testLeft() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);

        assertEquals("LinkedBinaryTreeTests.testLeft", 20, lbt.left(root).getElement());
    }

    private static void testRight() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addRight(root, 20);

        assertEquals("LinkedBinaryTreeTests.testRight", 20, lbt.right(root).getElement());
    }

    private static void testRoot() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);

        assertEquals("LinkedBinaryTreeTests.testRoot", 10, lbt.root().getElement());
    }

    private static void testParent() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);

        assertEquals("LinkedBinaryTreeTests.testParent", 60, lbt.parent(node6).getElement());
    }

    private static void testSize() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);

        assertEquals("LinkedBinaryTreeTests.testSize", 7, lbt.size());
    }

    private static void testNodes() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);
        String s = "10 20 30 40 50 60 70 ";
        StringBuilder sb = new StringBuilder();

        for (Node node : lbt.nodes()
                ) {
            sb.append(node.getElement()).append(" ");
        }
        assertEquals("LinkedBinaryTreeTests.testNodes", s, sb.toString());
    }

    private static void testPreOrder() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);

        String s = "10 30 60 70 20 50 40 ";
        StringBuilder sb = new StringBuilder();

        for (Node node : lbt.preOrder()
                ) {
            sb.append(node.getElement()).append(" ");
        }
        assertEquals("LinkedBinaryTreeTests.testPreOrder", s, sb.toString());
    }

    private static void testInOrder() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);

        String s = "40 20 50 10 70 60 30 ";
        StringBuilder sb = new StringBuilder();

        for (Node node : lbt.inOrder()
                ) {
            sb.append(node.getElement()).append(" ");
        }
        assertEquals("LinkedBinaryTreeTests.testInOrder", s, sb.toString());
    }

    private static void testPostOrder() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);

        String s = "40 50 20 70 60 30 10 ";
        StringBuilder sb = new StringBuilder();

        for (Node node : lbt.postOrder()
                ) {
            sb.append(node.getElement()).append(" ");
        }

        assertEquals("LinkedBinaryTreeTests.testPostOrder", s, sb.toString());
    }

    private static void testBreadthFirst() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(10);
        Node<Integer> node1 = lbt.addLeft(root, 20);
        Node<Integer> node2 = lbt.add(root, 30);
        Node<Integer> node3 = lbt.add(node1, 40);
        Node<Integer> node4 = lbt.add(node1, 50);
        Node<Integer> node5 = lbt.addLeft(node2, 60);
        Node<Integer> node6 = lbt.addLeft(node5, 70);

        String s = "10 20 30 40 50 60 70 ";
        StringBuilder sb = new StringBuilder();

        for (Node node : lbt.breadthFirst()) {
            sb.append(node.getElement()).append(" ");
        }
        assertEquals("LinkedBinaryTreeTests.testBreadthFirst", s, sb.toString());
    }
}
