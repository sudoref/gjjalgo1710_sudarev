package com.getjavajob.training.algo1710.sudarevr.lesson05;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<E> {
    Node<E> last;
    Node<E> head;
    int size;

    void addFirst(E e) {
        Node<E> newNode = new Node<>(e);
        newNode.next = head;
        head = newNode;
        size++;
    }

    E removeFirst() {
        Node<E> oldVal = head;
        head = head.next;
        size--;
        return oldVal.val;
    }

    E removeLast() {
        Node<E> x = head;
        last = x;
        for (int i = 0; i < size; i++) {
            x = x.next;
        }
        last = null;
        return x.val;
    }

    E get(int index) {
        Node<E> x = head;
        for (int i = 0; i < index; i++) {
            x = x.next;
        }
        return x.val;
    }

    int size() {
        return size;
    }

    List<E> asList() {
        List<E> list = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            list.add(get(i));
        }
        return list;
    }

    static class Node<E> {
        Node<E> next;
        E val;

        public Node(E val) {
            this.val = val;
        }
    }
}

