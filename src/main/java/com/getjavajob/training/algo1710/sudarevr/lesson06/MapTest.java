package com.getjavajob.training.algo1710.sudarevr.lesson06;


import com.getjavajob.training.algo1710.util.Assert;

import java.util.HashMap;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        mapIsEmptyTest();
        mapPutTest();
        mapGetTest();
        mapRemoveTest();
        mapContainsKeyTest();
        mapContainsValueTest();
        mapPutAllTest();
        mapClearTest();
        mapKeySet();
        mapValues();
        mapGetOrDefault();
        mapPutIfAbsent();
        mapReplace();
    }

    private static void mapIsEmptyTest() {
        Map<String, Integer> map = new HashMap<>();
        Assert.assertEquals("mapIsEmptyTestTrue", true, map.isEmpty());
        map.put("1", 1);
        Assert.assertEquals("mapIsEmptyTestFalse", false, map.isEmpty());
    }

    private static void mapPutTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        Assert.assertEquals("mapPutTest", 1, map.put("1", 1));
    }

    private static void mapGetTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        Assert.assertEquals("mapGetTest", 1, map.get("1"));
    }

    private static void mapRemoveTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        Assert.assertEquals("mapRemoveTest", 1, map.remove("1"));
    }

    private static void mapContainsKeyTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        Assert.assertEquals("mapContainsKeyTestTrue", true, map.containsKey("1"));
        Assert.assertEquals("mapContainsKeyTestFalse", false, map.containsKey("2"));
    }

    private static void mapContainsValueTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        Assert.assertEquals("mapContainsValueTest", true, map.containsValue(1));
        Assert.assertEquals("mapContainsValueTestFalse", false, map.containsValue(2));
    }

    private static void mapPutAllTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        Map<String, Integer> testMap = new HashMap<>();
        testMap.putAll(map);
        Assert.assertEquals("mapPutAllTest", 1, testMap.get("2"));
    }

    private static void mapClearTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        map.clear();
        Assert.assertEquals("mapClearTest", true, map.isEmpty());
    }

    private static void mapKeySet() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        String[] arr = {"1", "2"};
        Assert.assertEquals("mapKeySet", arr, map.keySet().toArray());
    }

    private static void mapValues() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        Integer[] arr = {2, 1};
        Assert.assertEquals("mapValuesTest", arr, map.values().toArray());
    }

    private static void mapGetOrDefault() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        Assert.assertEquals("mapGetOrDefault", 1, map.getOrDefault("2", 1));
    }

    private static void mapPutIfAbsent() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        Assert.assertEquals("mapPutIfAbsent", 1, map.putIfAbsent("2", 10));
    }

    private static void mapReplace() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 2);
        map.put("2", 1);
        map.replace("1", 2, 10);
        Assert.assertEquals("mapReplace", 10, map.get("1"));
    }

}
