package com.getjavajob.training.algo1710.sudarevr.lesson04;

import com.getjavajob.training.algo1710.util.StopWatch;

import java.util.LinkedList;

public class DLLPerformanceTest {
    public static void main(String[] args) {

        //testAddBegin();
        //testDLLAddMiddle();
        //testLLAddMiddle();
        //testDLLAddEnd();
        //testLLAddEnd();
        //testDLLRemoveBegin();
        //testLLRemoveBegin();
        //testDLLRemoveMiddle();
        //testLLRemoveMiddle();
        //testDLLRemoveEnd();
        testLLRemoveEnd();

    }

    public static void testAddBegin() {
        StopWatch sw = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        LinkedList<Integer> ll = new LinkedList<>();

        System.out.println("-------- Addition to the beginning --------");

        sw.start();
        for (int i = 0; i < 30000000; i++) {
            dll.add(0, i);
        }
        System.out.println("LinkedList.add(0,i): " + sw.getElapsedTime() + " ms");
        sw.start();
        for (int i = 0; i < 30000000; i++) {
            ll.add(0, i);
        }
        System.out.println("DoublyLinkedList.add(0,i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testDLLAddMiddle() {
        StopWatch sw = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        dll.add(1);
        dll.add(2);
        dll.add(3);


        System.out.println("-------- Addition to the middle DoublyLinkedList --------");

        sw.start();
        for (int i = 2; i < 10000000; i++) {
            dll.add(i - 1, i);
        }
        System.out.println("DoublyLinkedList.add(0,i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testLLAddMiddle() {
        StopWatch sw = new StopWatch();
        LinkedList<Integer> ll = new LinkedList<>();
        ll.add(1);
        ll.add(2);
        ll.add(3);

        System.out.println("-------- Addition to the middle LinkedList--------");

        sw.start();
        for (int i = 2; i < 10000000; i++) {
            ll.add(i - 1, i);
        }
        System.out.println("LinkedList.add(0,i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testDLLAddEnd() {
        StopWatch sw = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        dll.add(1);
        dll.add(2);
        dll.add(3);

        System.out.println("-------- Addition to the end DoublyLinkedList--------");
        sw.start();
        for (int i = 0; i < 10000000; i++) {
            dll.add(i);
        }
        System.out.println("DoublyLinkedList.add(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testLLAddEnd() {
        StopWatch sw = new StopWatch();
        LinkedList<Integer> ll = new LinkedList<>();
        ll.add(1);
        ll.add(2);
        ll.add(3);

        System.out.println("-------- Addition to the end LinkedList--------");
        sw.start();
        for (int i = 0; i < 10000000; i++) {
            ll.add(i);
        }
        System.out.println("LinkedList.add(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testDLLRemoveBegin() {
        StopWatch sw = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();

        for (int i = 0; i < 200000; i++) {
            dll.add(i);
        }

        System.out.println("-------- Removing from begin DoublyLinkedList--------");
        sw.start();
        for (int i = 0; i < 80000; i++) {
            dll.remove(i);
        }
        System.out.println("DoublyLinkedList.remove(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testLLRemoveBegin() {
        StopWatch sw = new StopWatch();
        LinkedList<Integer> ll = new LinkedList<>();

        for (int i = 0; i < 200000; i++) {
            ll.add(i);
        }

        System.out.println("-------- Removing from begin LinkedList--------");
        sw.start();
        for (int i = 0; i < 80000; i++) {
            ll.remove(i);
        }
        System.out.println("LinkedList.remove(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testDLLRemoveMiddle() {
        StopWatch sw = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();

        for (int i = 0; i < 200000; i++) {
            dll.add(i);
        }

        System.out.println("-------- Removing from middle DoublyLinkedList--------");
        sw.start();
        for (int i = 40000; i < 80000; i++) {
            dll.remove(i);
        }
        System.out.println("DoublyLinkedList.remove(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testLLRemoveMiddle() {
        StopWatch sw = new StopWatch();
        LinkedList<Integer> ll = new LinkedList<>();

        for (int i = 0; i < 200000; i++) {
            ll.add(i);
        }

        System.out.println("-------- Removing from middle LinkedList--------");
        sw.start();
        for (int i = 40000; i < 80000; i++) {
            ll.remove(i);
        }
        System.out.println("LinkedList.remove(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testDLLRemoveEnd() {
        StopWatch sw = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();

        for (int i = 0; i < 40000000; i++) {
            dll.add(i);
        }

        System.out.println("-------- Removing from end DoublyLinkedList--------");
        sw.start();
        for (int i = dll.size - 1; i > 0; i--) {
            dll.remove(i);
        }
        System.out.println("DoublyLinkedList.remove(i): " + sw.getElapsedTime() + " ms\n");
    }

    public static void testLLRemoveEnd() {
        StopWatch sw = new StopWatch();
        LinkedList<Integer> ll = new LinkedList<>();

        for (int i = 0; i < 40000000; i++) {
            ll.add(i);
        }

        System.out.println("-------- Removing from endLinkedList--------");
        sw.start();
        for (int i = ll.size() - 1; i > 0; i--) {
            ll.remove(i);
        }
        System.out.println("LinkedList.remove(i): " + sw.getElapsedTime() + " ms\n");
    }
}
