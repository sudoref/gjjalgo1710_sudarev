package com.getjavajob.training.algo1710.sudarevr.lesson10;

public class MergeSort {
    static Comparable [] aux;
    static Comparable[] sort(Comparable[] a){
        aux = new Comparable[a.length];
        return sort(a,0,a.length-1);
    }

    static Comparable[] sort(Comparable[]a, int lo, int hi){
        if (lo>=hi) return a;
        int mid = lo+(hi-lo)/2;
        sort(a,lo,mid);
        sort(a,mid+1, hi);
        merge(a,lo,mid,hi);
        return a;
    }

    private static void merge(Comparable[] a, int lo, int mid, int hi) {
        int i =lo;
        int j = mid+1;
        for(int k = lo; k<=hi;k++){
            aux[k] = a[k];
        }
        for(int k = lo; k<=hi; k++){
            if(i>mid){
                a[k]=aux[j++];
            } else if(j>hi){
                a[k]=aux[i++];
            } else if(less(a,j,i)){
                a[k] = aux[j++];
            } else {
                a[k] = aux[i++];
            }
        }
    }

    private static boolean less(Comparable[] a, int i, int j) {
        return a[i].compareTo(a[j]) < 0;
    }
}
