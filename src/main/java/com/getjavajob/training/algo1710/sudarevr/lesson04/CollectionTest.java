package com.getjavajob.training.algo1710.sudarevr.lesson04;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionTest {
    public static void main(String[] args) {
        testAddCol();
        testIsEmptyCol();
        testSizeCol();
        testContainsCol();
        testEqualsCol();
        testAddAllCol();
        testContainsAllCol();
        testClearCol();
        testRemoveAll();
        testRetainAllCol();
        testToArrayCol();
    }

    public static void testAddCol() {
        Collection<Integer> col = new ArrayList<>();
        Assert.assertEquals("testAddCol()", true, col.add(1));
    }

    public static void testIsEmptyCol() {
        Collection<Integer> col = new ArrayList<>();
        Assert.assertEquals("testIsEmptyCol(true)", true, col.isEmpty());
        col.add(1);
        Assert.assertEquals("testIsEmptyCol(false)", false, col.isEmpty());
    }

    public static void testSizeCol() {
        Collection<Integer> col = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            col.add(i);
        }
        Assert.assertEquals("testSizeCol()", 10, col.size());
    }

    public static void testContainsCol() {
        Collection<String> col = new ArrayList<>();
        col.add("test");
        Assert.assertEquals("testContainsCol(true)", true, col.contains("test"));
        Assert.assertEquals("testContainsCol(false)", false, col.contains("est"));
    }

    public static void testEqualsCol() {
        Collection<String> col = new ArrayList<>();
        col.add("test");
        ArrayList al = new ArrayList();
        al.add("test");
        ArrayList al2 = new ArrayList();
        al2.add("test2");
        Assert.assertEquals("testEqualsCol(true)", true, col.equals(al));
        Assert.assertEquals("testEqualsCol(false)", false, col.equals(al2));
    }

    public static void testAddAllCol() {
        Collection<String> col = new ArrayList<>();
        ArrayList al = new ArrayList();
        al.add("test");
        Assert.assertEquals("testAddAllCol(true)", true, col.addAll(al));
    }

    public static void testContainsAllCol() {
        Collection<String> col = new ArrayList<>();
        col.add("test");
        col.add("test2");
        ArrayList al = new ArrayList();
        al.add("test");
        al.add("test2");
        Assert.assertEquals("testContainsAllCol(true)", true, col.containsAll(al));
        col.remove("test");
        Assert.assertEquals("testContainsAllCol(false)", false, col.containsAll(al));
    }

    public static void testClearCol() {
        Collection<String> col = new ArrayList<>();
        col.add("test");
        col.add("test2");
        Assert.assertEquals("testClearCol(false)", false, col.isEmpty());
        col.clear();
        Assert.assertEquals("testClearCol(true)", true, col.isEmpty());
    }

    public static void testRemoveAll() {
        Collection<String> col = new ArrayList<>();
        ArrayList<String> al = new ArrayList<>();
        al.add("test");
        al.add("test2");
        al.add("test3");
        col.addAll(al);
        col.add("test4");
        Assert.assertEquals("testRemoveAll()", true, col.removeAll(al));
    }

    public static void testRetainAllCol() {
        Collection<String> col = new ArrayList<>();
        ArrayList<String> al = new ArrayList<>();
        al.add("test");
        al.add("test2");
        al.add("test3");
        col.addAll(al);
        col.add("test4");
        Assert.assertEquals("testRetainAllCol()", true, col.retainAll(al));
    }

    public static void testToArrayCol() {
        Collection<Integer> col = new ArrayList<>();
        Object[] arr = {1, 5, 8};
        col.add(1);
        col.add(5);
        col.add(8);
        Assert.assertEquals(" testToArrayCol()", arr, col.toArray());
    }

}
