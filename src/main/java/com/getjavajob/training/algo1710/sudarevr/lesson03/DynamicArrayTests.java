package com.getjavajob.training.algo1710.sudarevr.lesson03;

import com.getjavajob.training.algo1710.util.Assert;

public class DynamicArrayTests {
    public static void main(String[] args) {
        testExceptionNewArray();
        testExceptionAdd();
        testExceptionGet();
        testExceptionSet();
        testBooleanAdd();
        testAdd();
        testSet();
        testGet();
        testBooleanRemove();
        testRemove();
        testSize();
        testIndexOf();
        testContains();
    }

    static void testExceptionAdd() {
        try {
            DynamicArray da = new DynamicArray();
            da.add(-1, 2);
            Assert.fail("DynamicArrayTests.testExceptionAdd() failed"); // that will throw java.lang.AssertionError
        } catch (Exception e) {
            Assert.assertEquals("DynamicArrayTests.testExceptionAdd()", "index is negative", e.getMessage());
        }
    }

    static void testExceptionNewArray() {
        try {
            DynamicArray da = new DynamicArray(-1);
            Assert.fail("DynamicArrayTests.testExceptionNewArray() failed");
        } catch (Exception e) {
            Assert.assertEquals("DynamicArrayTests.testExceptionNewArray()", "IllegalArgumentException", e.getMessage());
        }
    }

    static void testExceptionGet() {
        try {
            DynamicArray da = new DynamicArray();
            da.add(1);
            da.get(10);
            Assert.fail("DynamicArrayTests.testExceptionGet() failed");
        } catch (Exception e) {
            Assert.assertEquals("DynamicArrayTests.testExceptionGet()", "out of bound index: 10, size 1", e.getMessage());
        }
    }

    static void testExceptionSet() {
        try {
            DynamicArray da = new DynamicArray();
            da.set(-1, 10);
            Assert.fail("DynamicArrayTests.testExceptionSet() failed");
        } catch (Exception e) {
            Assert.assertEquals("DynamicArrayTests.testExceptionSet()", "index is negative", e.getMessage());
        }
    }

    static void testBooleanAdd() {
        DynamicArray da = new DynamicArray();
        Assert.assertEquals("DynamicArrayTests.testBooleanAdd", true, da.add(0));
    }

    static void testAdd() {
        DynamicArray da = new DynamicArray();
        Object[] arr = new Object[3];
        for (int i = 0; i < 3; i++) {
            da.add(i, i);
            arr[i] = i;
        }
        Object[] arr2 = da.toArray();
        Assert.assertEquals("DynamicArrayTests.testAdd", arr, arr2);
    }

    static void testSet() {
        DynamicArray da = new DynamicArray();
        da.add("testSet");
        Assert.assertEquals("DynamicArrayTests.testSet", "testSet", (String) da.set(0, 1));
    }

    static void testGet() {
        DynamicArray da = new DynamicArray();
        da.add("testGet");
        Assert.assertEquals("DynamicArrayTests.testGet", "testGet", (String) da.get(0));
    }

    static void testBooleanRemove() {
        DynamicArray da = new DynamicArray();
        da.add("testBooleanRemove");
        Assert.assertEquals("DynamicArrayTests.testBooleanRemove(true)", true, da.remove("testBooleanRemove"));
        Assert.assertEquals("DynamicArrayTests.testBooleanRemove(false)", false, da.remove(true));
    }

    static void testRemove() {
        DynamicArray da = new DynamicArray();
        da.add("testRemoveList");
        for (int i = 0; i < 10; i++) {
            da.add(i);
        }
        Assert.assertEquals("DynamicArrayTests.testRemoveList(beginning)", "testRemoveList", (String) da.remove(0));
        Assert.assertEquals("DynamicArrayTests.testRemoveList(middle)", 5, (Integer) da.remove(5));
        Assert.assertEquals("DynamicArrayTests.testRemoveList(end)", 9, (Integer) da.remove(da.size() - 1));
    }

    static void testSize() {
        DynamicArray da = new DynamicArray();
        da.add("testSllSize");
        for (int i = 0; i < 10; i++) {
            da.add(i);
        }
        Assert.assertEquals("DynamicArrayTests.testSllSize", 11, da.size());
    }

    static void testIndexOf() {
        DynamicArray da = new DynamicArray();
        da.add("testIndexOf");
        for (int i = 0; i < 10; i++) {
            da.add(i);
        }
        Assert.assertEquals("DynamicArrayTests.testIndexOf", 0, da.indexOf("testIndexOf"));
    }

    static void testContains() {
        DynamicArray da = new DynamicArray();
        da.add("testContains");
        Assert.assertEquals("DynamicArrayTests.testContains(true)", true, da.contains("testContains"));
        Assert.assertEquals("DynamicArrayTests.testContains(false)", false, da.contains("false"));
    }

}

