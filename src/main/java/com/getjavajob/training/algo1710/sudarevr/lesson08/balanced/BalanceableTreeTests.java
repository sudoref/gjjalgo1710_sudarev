package com.getjavajob.training.algo1710.sudarevr.lesson08.balanced;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;
import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary.LinkedBinaryTree;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;


public class BalanceableTreeTests extends LinkedBinaryTree {
    public static void main(String[] args) {
        testRotate();
        testReduceSubtreeHeight();
        testToString();
        testDrawTheTree();
    }

    private static void testRotate() {
        BalanceableTree<Integer> balanceableTree = new BalanceableTree<>();
        Node<Integer> nodeQ = balanceableTree.addRoot(10);
        Node<Integer> nodeP = balanceableTree.addLeft(nodeQ, 5);
        Node<Integer> nodeC = balanceableTree.addRight(nodeQ, 90);
        Node<Integer> nodeA = balanceableTree.addLeft(nodeP, 3);
        Node<Integer> nodeB = balanceableTree.addRight(nodeP, 7);
        balanceableTree.rotate(nodeP);

        StringBuilder sb = new StringBuilder();
        for (Node node : balanceableTree.breadthFirst()) {
            sb.append(node.getElement()).append(" ");
        }
        assertEquals("testRotate", "5 3 10 7 90 ", sb.toString());
    }

    private static void testReduceSubtreeHeight() {
        BalanceableTree<Integer> balanceableTree = new BalanceableTree<>();
        Node<Integer> nodeQ = balanceableTree.addRoot(10);
        Node<Integer> nodeP = balanceableTree.addLeft(nodeQ, 5);
        Node<Integer> nodeC = balanceableTree.addRight(nodeQ, 90);
        Node<Integer> nodeA = balanceableTree.addLeft(nodeP, 3);
        Node<Integer> nodeB = balanceableTree.addRight(nodeA, 4);

        balanceableTree.reduceSubtreeHeight(nodeB);

        StringBuilder sb = new StringBuilder();
        for (Node node : balanceableTree.breadthFirst()) {
            sb.append(node.getElement()).append(" ");
        }
        assertEquals("testReduceSubtreeHeight", "10 4 90 3 5 ", sb.toString());
    }

    private static void testToString() {
        BalanceableTree<Integer> balanceableTree = new BalanceableTree<>();
        Node<Integer> nodeQ = balanceableTree.addRoot(10);
        Node<Integer> nodeP = balanceableTree.addLeft(nodeQ, 5);
        Node<Integer> nodeC = balanceableTree.addRight(nodeQ, 90);
        Node<Integer> nodeA = balanceableTree.addLeft(nodeP, 3);
        Node<Integer> nodeB = balanceableTree.addRight(nodeP, 7);

        String s = balanceableTree.toString();
        assertEquals("testToString", "(10(5(3)(7))(90))", s);
    }

    private static void testDrawTheTree() {
        BalanceableTree<Integer> balanceableTree = new BalanceableTree<>();
        Node<Integer> nodeQ = balanceableTree.addRoot(10);
        Node<Integer> nodeP = balanceableTree.addLeft(nodeQ, 5);
        Node<Integer> nodeC = balanceableTree.addRight(nodeQ, 90);
        Node<Integer> nodeA = balanceableTree.addLeft(nodeP, 3);
        Node<Integer> nodeB = balanceableTree.addRight(nodeP, 7);

        balanceableTree.drawTheTree(nodeQ);
    }
}
