package com.getjavajob.training.algo1710.sudarevr.lesson05;

import com.getjavajob.training.algo1710.util.Assert;


public class LinkedListStackTest {
    public static void main(String[] args) {
        testPush();
        testPop();
        testPeek();
        testEmpty();
        testAsList();
    }

    public static void testPush() {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("1");
        Assert.assertEquals("LinkedListStackTest.TestPush", "1", lls.peek());
    }

    public static void testPop() {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("1");
        lls.push("2");
        Assert.assertEquals("LinkedListStackTest.TestPop", "2", lls.pop());
    }

    public static void testPeek() {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("1");
        lls.push("2");
        Assert.assertEquals("LinkedListStackTest.TestPeek", "1", lls.peek());
    }

    public static void testEmpty() {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("1");
        Assert.assertEquals("LinkedListStackTest.testEmpty(false)", false, lls.empty());
        lls.pop();
        Assert.assertEquals("LinkedListStackTest.testEmpty(true)", true, lls.empty());
    }

    public static void testAsList() {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("1");
        lls.push("2");
        lls.push("3");
        Object[] arr2 = {"3", "2", "1"};
        Assert.assertEquals("LinkedListStackTest.asList", arr2, lls.asList().toArray());
    }
}
