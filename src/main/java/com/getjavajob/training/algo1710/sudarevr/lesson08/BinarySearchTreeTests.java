package com.getjavajob.training.algo1710.sudarevr.lesson08;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class BinarySearchTreeTests {
    public static void main(String[] args) {
        testCompare();
        testTreeSearch();
    }

    private static void testCompare() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        Node<Integer> nodeQ = bst.addRoot(10);
        Node<Integer> nodeP = bst.addLeft(nodeQ, 5);

        assertEquals("testCompare", 1, bst.compare(nodeQ.getElement(), nodeP.getElement()));
    }

    private static void testTreeSearch() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        Node<Integer> nodeQ = bst.addRoot(10);
        Node<Integer> nodeP = bst.addLeft(nodeQ, 5);
        Node<Integer> nodeC = bst.addRight(nodeQ, 90);
        Node<Integer> nodeA = bst.addLeft(nodeP, 3);
        Node<Integer> nodeB = bst.addRight(nodeP, 7);

        assertEquals("testTreeSearch", 7, bst.treeSearch(nodeP, 7).getElement());
    }
}
