package com.getjavajob.training.algo1710.sudarevr.lesson05;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.ArrayDeque;
import java.util.Queue;

public class QueueTest {
    public static void main(String[] args) {
        queueTestAdd();
        queueTestOffer();
        queueTestRemove();
        queueTestPoll();
        queueTestElement();
        queueTestPeek();
    }

    public static void queueTestAdd() {
        Queue<String> queue = new ArrayDeque<>();
        Assert.assertEquals("QueueTest.queueTestAdd()", true, queue.add("1"));
    }

    public static void queueTestOffer() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("1");
        queue.add("2");
        Assert.assertEquals("QueueTest.queueTestOffer()", true, queue.offer("3"));
    }

    public static void queueTestRemove() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("1");
        queue.add("2");
        Assert.assertEquals("QueueTest.queueTestRemove()", "1", queue.remove());
    }

    public static void queueTestPoll() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("1");
        queue.add("2");
        Assert.assertEquals("QueueTest.queueTestPool()", "1", queue.poll());
    }

    public static void queueTestElement() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("1");
        queue.add("2");
        Assert.assertEquals("QueueTest.queueTestElement()", "1", queue.element());
    }

    public static void queueTestPeek() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("1");
        queue.add("2");
        Assert.assertEquals("QueueTest.queueTestPeek()", "1", queue.peek());
    }
}
