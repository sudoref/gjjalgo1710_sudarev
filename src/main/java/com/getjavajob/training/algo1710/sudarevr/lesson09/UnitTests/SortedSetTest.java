package com.getjavajob.training.algo1710.sudarevr.lesson09.UnitTests;


import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class SortedSetTest {
    public static void main(String[] args) {
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    private static void testSubSet() {
        SortedSet<Integer> sortedSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            sortedSet.add(i);
        }
        assertEquals("testSubSet", "[5, 6, 7, 8]", sortedSet.subSet(5, 9).toString());
    }

    private static void testHeadSet() {
        SortedSet<Integer> sortedSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            sortedSet.add(i);
        }
        assertEquals("testHeadSet", "[0, 1, 2, 3, 4]", sortedSet.headSet(5).toString());
    }

    private static void testTailSet() {
        SortedSet<Integer> sortedSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            sortedSet.add(i);
        }
        assertEquals("testTailSet", "[5, 6, 7, 8, 9]", sortedSet.tailSet(5).toString());
    }

    private static void testFirst() {
        SortedSet<Integer> sortedSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            sortedSet.add(i);
        }
        assertEquals("testFirst", 0, sortedSet.first());
    }

    private static void testLast() {
        SortedSet<Integer> sortedSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            sortedSet.add(i);
        }
        assertEquals("testLast", 9, sortedSet.last());
    }

}
