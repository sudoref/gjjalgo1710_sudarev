package com.getjavajob.training.algo1710.sudarevr.lesson09.RedBlackTree;


import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class RedBlackTreeTests {
    public static void main(String[] args) {
        testAdd();
        testRemove();
        testIsBlack();
        testIsRed();
    }

    private static void testAdd() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> nodeQ = tree.addRoot(10);
        Node<Integer> nodeP = tree.add(tree.root(), 11);
        Node<Integer> nodeA = tree.add(tree.root(), 8);
        Node<Integer> nodeB = tree.add(tree.root(), 7);
        Node<Integer> nodeC = tree.add(tree.root(), 6);
        Node<Integer> nodeD = tree.add(tree.root(), 9);
        Node<Integer> nodeE = tree.add(tree.root(), 5);
        Node<Integer> nodeF = tree.add(tree.root(), 3);

        assertEquals("testAdd", "(10(7(5(3)(6))(8(9)))(11))", tree.toString());
    }

    private static void testRemove() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> nodeQ = tree.addRoot(10);
        Node<Integer> nodeP = tree.add(tree.root(), 11);
        Node<Integer> nodeA = tree.add(tree.root(), 8);
        Node<Integer> nodeB = tree.add(tree.root(), 7);
        Node<Integer> nodeC = tree.add(tree.root(), 6);
        Node<Integer> nodeD = tree.add(tree.root(), 9);
        Node<Integer> nodeE = tree.add(tree.root(), 5);
        Node<Integer> nodeF = tree.add(tree.root(), 3);

        tree.remove(nodeB);

        assertEquals("testRemove", "(10(6(5(3))(8(9)))(11))", tree.toString());
    }

    private static void drawTheTree() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> nodeQ = tree.addRoot(10);
        Node<Integer> nodeP = tree.add(tree.root(), 11);
        Node<Integer> nodeA = tree.add(tree.root(), 8);
        Node<Integer> nodeB = tree.add(tree.root(), 7);
        Node<Integer> nodeC = tree.add(tree.root(), 6);
        Node<Integer> nodeD = tree.add(tree.root(), 9);
        Node<Integer> nodeE = tree.add(tree.root(), 5);
        Node<Integer> nodeF = tree.add(tree.root(), 3);

        tree.drawTheTree(nodeQ);
    }

    private static void testIsBlack() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> nodeQ = tree.addRoot(10);
        Node<Integer> nodeP = tree.add(tree.root(), 11);
        Node<Integer> nodeA = tree.add(tree.root(), 8);
        Node<Integer> nodeB = tree.add(tree.root(), 7);
        Node<Integer> nodeC = tree.add(tree.root(), 6);
        Node<Integer> nodeD = tree.add(tree.root(), 9);
        Node<Integer> nodeE = tree.add(tree.root(), 5);
        Node<Integer> nodeF = tree.add(tree.root(), 3);
        drawTheTree();
        System.out.println();
        assertEquals("testIsBlackFalse", false, tree.isBlack(nodeB));
        assertEquals("testIsBlackTrue", true, tree.isBlack(nodeA));
    }

    private static void testIsRed() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> nodeQ = tree.addRoot(10);
        Node<Integer> nodeP = tree.add(tree.root(), 11);
        Node<Integer> nodeA = tree.add(tree.root(), 8);
        Node<Integer> nodeB = tree.add(tree.root(), 7);
        Node<Integer> nodeC = tree.add(tree.root(), 6);
        Node<Integer> nodeD = tree.add(tree.root(), 9);
        Node<Integer> nodeE = tree.add(tree.root(), 5);
        Node<Integer> nodeF = tree.add(tree.root(), 3);

        System.out.println();
        assertEquals("testIsRedTrue", true, tree.isRed(nodeC));
        assertEquals("testIsRedFalse", false, tree.isRed(nodeP));
    }
}
