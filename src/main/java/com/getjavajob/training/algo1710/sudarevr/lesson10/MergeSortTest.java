package com.getjavajob.training.algo1710.sudarevr.lesson10;

import static com.getjavajob.training.algo1710.sudarevr.lesson10.MergeSort.sort;
import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class MergeSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        Comparable[] arr = {5, 2, 7, 1, 8};
        Comparable[] sortedArr = {1, 2, 5, 7, 8};

        assertEquals("testSort", sortedArr, sort(arr));
    }
}
