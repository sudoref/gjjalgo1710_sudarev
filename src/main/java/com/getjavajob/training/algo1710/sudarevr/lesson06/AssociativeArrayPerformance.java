package com.getjavajob.training.algo1710.sudarevr.lesson06;

import com.getjavajob.training.algo1710.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

public class AssociativeArrayPerformance {
    public static void main(String[] args) {
        //testAssociativeArrayAdd();
        //testHashMapAdd();
        //testAssociativeArrayRemove();
        //testHashMapRemove();
        //testAssociativeArrayGet();
        testHashMapGet();
    }

    private static void testAssociativeArrayAdd() {
        StopWatch sw = new StopWatch();
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();

        System.out.println("---------Addition test AssociativeArray ------------");
        sw.start();
        for (int i = 0; i < 10000000; i++) {
            aa.add("" + i, i);
        }
        System.out.println("addition 1 mln objects " + sw.getElapsedTime() + " ms");
    }

    private static void testHashMapAdd() {
        StopWatch sw = new StopWatch();
        HashMap<String, Integer> hashMap = new HashMap<>();

        System.out.println("---------Addition test HashMap------------");
        sw.start();
        for (int i = 0; i < 10000000; i++) {
            hashMap.put("" + i, i);
        }
        System.out.println("addition 1 mln objects " + sw.getElapsedTime() + " ms");
    }

    private static void testAssociativeArrayRemove() {
        StopWatch sw = new StopWatch();
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        for (int i = 0; i < 10000000; i++) {
            aa.add("" + i, i);
        }
        System.out.println("---------Removing test AssociativeArray------------");
        sw.start();

        for (int i = aa.size() - 1; i > 0; i--) {
            aa.remove("" + i);
        }
        System.out.println("removing 10 mln objects " + sw.getElapsedTime() + " ms");

    }

    private static void testHashMapRemove() {
        StopWatch sw = new StopWatch();
        Map<String, Integer> hm = new HashMap<>();
        for (int i = 0; i < 10000000; i++) {
            hm.put("" + i, i);
        }
        System.out.println("---------Removing test HashMap------------");
        sw.start();

        for (int i = hm.size() - 1; i > 0; i--) {
            hm.remove("" + i);
        }
        System.out.println("removing 10 mln objects " + sw.getElapsedTime() + " ms");
    }

    private static void testAssociativeArrayGet() {
        StopWatch sw = new StopWatch();
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        for (int i = 0; i < 10000000; i++) {
            aa.add("" + i, i);
        }
        System.out.println("---------Get test AssociativeArray------------");
        sw.start();

        for (int i = aa.size() - 1; i > 0; i--) {
            aa.get("" + i);
        }
        System.out.println("removing 10 mln objects " + sw.getElapsedTime() + " ms");
    }

    private static void testHashMapGet() {
        StopWatch sw = new StopWatch();
        Map<String, Integer> hm = new HashMap<>();
        for (int i = 0; i < 10000000; i++) {
            hm.put("" + i, i);
        }
        System.out.println("---------get test HashMap------------");
        sw.start();

        for (int i = hm.size() - 1; i > 0; i--) {
            hm.get("" + i);
        }
        System.out.println("get 10 mln objects " + sw.getElapsedTime() + " ms");
    }
}
