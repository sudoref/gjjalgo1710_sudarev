package com.getjavajob.training.algo1710.sudarevr.lesson09.UnitTests;

import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class NavigableSetTest {
    public static void main(String[] args) {
        testLower();
        testFloor();
        testCeiling();
        testHigher();
        testPollFirst();
        testPollLast();
        testDescendingSet();
        testDescendingIterator();
        testSubSet();
        testHeadSet();
        testTailSet();
    }

    private static void testTailSet() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testTailSet", "[6, 7, 8, 9]", navigableSet.tailSet(5, false).toString());
    }

    private static void testHeadSet() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testHeadSet", "[0, 1, 2, 3, 4]", navigableSet.headSet(5, false).toString());
    }

    private static void testSubSet() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testSubSet", "[3, 4, 5, 6, 7, 8]", navigableSet.subSet(2, false, 8, true).toString());
    }

    private static void testDescendingIterator() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 2; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testDescendingIterator", 9, navigableSet.descendingIterator().next());
    }

    private static void testDescendingSet() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 2; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testDescendingSet", "[9, 8, 7, 6, 5, 4, 3, 2]", navigableSet.descendingSet().toString());
    }

    private static void testPollLast() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 2; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testPollLast", 9, navigableSet.pollLast());
    }

    private static void testPollFirst() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 2; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testPollFirst", 2, navigableSet.pollFirst());
    }

    private static void testHigher() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testHigher", 7, navigableSet.higher(6));
    }

    private static void testCeiling() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testCeiling", 8, navigableSet.ceiling(8));
    }

    private static void testFloor() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testFloor", 9, navigableSet.floor(55));
    }

    private static void testLower() {
        NavigableSet<Integer> navigableSet = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            navigableSet.add(i);
        }

        assertEquals("testLower", 4, navigableSet.lower(5));
    }


}
