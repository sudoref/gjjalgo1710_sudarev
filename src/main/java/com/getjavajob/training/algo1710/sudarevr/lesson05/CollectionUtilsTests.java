package com.getjavajob.training.algo1710.sudarevr.lesson05;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CollectionUtilsTests {
    public static void main(String[] args) {
        collectionUtilsFilter();
        collectionUtilsCollect();
        collectionUtilsTransform();
        collectionUtilsForAllDo();
    }

    private static void collectionUtilsFilter() {
        Employee e1 = new Employee(1, 23, "M", "Rick", "Beethovan");
        Employee e2 = new Employee(2, 13, "F", "Martina", "Hengis");
        Employee e3 = new Employee(3, 43, "M", "Ricky", "Martin");
        Employee e4 = new Employee(4, 26, "M", "Jon", "Lowman");
        Employee e5 = new Employee(5, 19, "F", "Cristine", "Maria");
        Employee e6 = new Employee(6, 15, "M", "David", "Roy");
        Employee e7 = new Employee(7, 68, "F", "Melissa", "Roy");
        Employee e8 = new Employee(8, 79, "M", "Alex", "Gussin");
        Employee e9 = new Employee(9, 15, "F", "Neetu", "Singh");
        Employee e10 = new Employee(10, 45, "M", "Naveen", "Jain");
        List<Employee> employeeList = new ArrayList<>(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10));
        List<Employee> employeesOlder21 = new ArrayList<>();
        for (Employee e : employeeList) {
            if (e.getAge() > 21) {
                employeesOlder21.add(e);
            }
        }
        CollectionUtils.filter(employeeList, employee -> employee.getAge() > 21);

        Assert.assertEquals("collectionUtilsFilter", employeesOlder21.toArray(), employeeList.toArray());
    }

    static void collectionUtilsCollect() {
        Employee e1 = new Employee(1, 23, "M", "Rick", "Beethovan");
        Employee e2 = new Employee(2, 13, "F", "Martina", "Hengis");
        Employee e3 = new Employee(3, 43, "M", "Ricky", "Martin");
        Employee e4 = new Employee(4, 26, "M", "Jon", "Lowman");
        Employee e5 = new Employee(5, 19, "F", "Cristine", "Maria");
        Employee e6 = new Employee(6, 15, "M", "David", "Roy");
        Employee e7 = new Employee(7, 68, "F", "Melissa", "Roy");
        Employee e8 = new Employee(8, 79, "M", "Alex", "Gussin");
        Employee e9 = new Employee(9, 15, "F", "Neetu", "Singh");
        Employee e10 = new Employee(10, 45, "M", "Naveen", "Jain");
        List<Employee> employeeList = new ArrayList<>();
        employeeList.addAll(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10));

        List<String> list = CollectionUtils.collect(employeeList, new Transformer<Employee, String>() {
            @Override
            public String transform(Employee var1) {
                return var1.getLastName();
            }
        }, new ArrayList<>());
        String[] arr = {"Beethovan", "Hengis", "Martin", "Lowman", "Maria", "Roy", "Roy", "Gussin", "Singh", "Jain"};
        Assert.assertEquals("collectionUtilsCollect(NO CHANGE COLLECTION)", arr, list.toArray());
    }

    static void collectionUtilsTransform() {
        Employee e1 = new Employee(1, 23, "M", "Rick", "Beethovan");
        Employee e2 = new Employee(2, 13, "F", "Martina", "Hengis");
        Employee e3 = new Employee(3, 43, "M", "Ricky", "Martin");
        Employee e4 = new Employee(4, 26, "M", "Jon", "Lowman");
        Employee e5 = new Employee(5, 19, "F", "Cristine", "Maria");
        Employee e6 = new Employee(6, 15, "M", "David", "Roy");
        Employee e7 = new Employee(7, 68, "F", "Melissa", "Roy");
        Employee e8 = new Employee(8, 79, "M", "Alex", "Gussin");
        Employee e9 = new Employee(9, 15, "F", "Neetu", "Singh");
        Employee e10 = new Employee(10, 45, "M", "Naveen", "Jain");
        List<Employee> employeeList = new ArrayList<>();
        employeeList.addAll(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10));

        CollectionUtils.transform(employeeList, new Transformer<Employee, String>() {
            @Override
            public String transform(Employee var1) {
                return var1.getLastName();
            }
        });
        String[] arr = {"Beethovan", "Hengis", "Martin", "Lowman", "Maria", "Roy", "Roy", "Gussin", "Singh", "Jain"};
        Assert.assertEquals("collectionUtilsTransform(CHANGED COLLECTION)", arr, employeeList.toArray());
    }

    static void collectionUtilsForAllDo() {
        Employee e1 = new Employee(1, 23, "M", "Rick", "Beethovan");
        Employee e2 = new Employee(2, 13, "F", "Martina", "Hengis");
        Employee e3 = new Employee(3, 43, "M", "Ricky", "Martin");
        Employee e4 = new Employee(4, 26, "M", "Jon", "Lowman");
        Employee e5 = new Employee(5, 19, "F", "Cristine", "Maria");
        Employee e6 = new Employee(6, 15, "M", "David", "Roy");
        Employee e7 = new Employee(7, 68, "F", "Melissa", "Roy");
        Employee e8 = new Employee(8, 79, "M", "Alex", "Gussin");
        Employee e9 = new Employee(9, 15, "F", "Neetu", "Singh");
        Employee e10 = new Employee(10, 45, "M", "Naveen", "Jain");
        List<Employee> employeeList = new ArrayList<>();
        employeeList.addAll(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10));

        List<Employee> newList = new ArrayList<>();
        for (Employee e : employeeList) {
            e.setAge(e.getAge() + 1);
            newList.add(e);
        }

        CollectionUtils.forAllDo(employeeList, new Closure<Employee>() {
            @Override
            public void execute(Employee var1) {
                var1.setAge(var1.getAge() + 1);
            }
        });
        Assert.assertEquals("collectionUtilsForAllDo()", newList.toArray(), employeeList.toArray());
    }


    static class Employee {
        private Integer id;
        private Integer age;
        private String gender;
        private String firstName;
        private String lastName;

        public Employee(Integer id, Integer age, String gender, String fName, String lName) {
            this.id = id;

            this.age = age;
            this.gender = gender;
            this.firstName = fName;
            this.lastName = lName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return this.id.toString() + " - " + this.age.toString();
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

    }
}
