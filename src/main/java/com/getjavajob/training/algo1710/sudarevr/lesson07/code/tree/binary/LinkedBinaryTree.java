package com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    // nonpublic utility
    private Node<E> root;
    private int size;

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        root = new NodeImpl<>(null, e, null, null);
        validate(root).setBlack(true);
        size++;
        return root;
    }

    public Node<E> addRoot(Node<E> node) throws IllegalStateException {
        validate(node).setBlack(true);
        root = node;
        size++;
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        } else if (left(n) == null) {
            return addLeft(n, e);
        } else if (right(n) == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        if (validate(n).getLeft() != null) {
            throw new IllegalArgumentException();
        }
        NodeImpl<E> newNode = new NodeImpl<>(null, e, null, validate(n));
        (validate(n)).setLeft(newNode);
        size++;
        return newNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        if (validate(n).getRight() != null) {
            throw new IllegalArgumentException();
        }
        NodeImpl<E> newNode = new NodeImpl<>(null, e, null, validate(n));
        (validate(n)).setRight(newNode);
        size++;
        return newNode;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        validate(n).setElement(e);
        return e;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> newNode = validate(n);
        if (n == root()) {
            throw new IllegalArgumentException("Root can't be deleted unambiguously");
        } else if (childrenNumber(n) > 1) {
            throw new IllegalArgumentException("In a binary tree, a node with two children cannot be deleted unambiguously");
        } else if (newNode.getLeft() != null) {
            if (validate(parent(newNode)).getLeft().equals(newNode)) {
                validate(parent(newNode)).setLeft(newNode.getLeft());
                newNode.getLeft().setParent((NodeImpl<E>) parent(newNode));
            } else {
                validate(parent(newNode)).setRight(newNode.getLeft());
                newNode.getLeft().setParent((NodeImpl<E>) parent(newNode));
            }
            return newNode.getElement();
        } else if (newNode.getRight() != null) {
            if (validate(parent(newNode)).getRight().equals(newNode)) {
                validate(parent(newNode)).setRight(newNode.getRight());
                newNode.getRight().setParent((NodeImpl<E>) parent(newNode));
            } else {
                validate(parent(newNode)).setLeft(newNode.getRight());
                newNode.getRight().setParent((NodeImpl<E>) parent(newNode));
            }
            return newNode.getElement();
        } else if (childrenNumber(n) == 0) {
            if (validate(parent(newNode)).getLeft().equals(newNode)) {
                validate(parent(newNode)).setLeft(null);
            } else {
                validate(parent(newNode)).setRight(null);
            }
            return newNode.getElement();
        } else {
            throw new IllegalArgumentException();
        }
    }

    // {@link Tree} and {@link BinaryTree} implementations
    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        if (p != null) {
            return validate(p).getLeft();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        if (p != null) {
            return validate(p).getRight();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> root() {
        return this.root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        return validate(n).getParent();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        List<E> list = new ArrayList<>();
        for (Node<E> node : nodes()) {
            list.add(node.getElement());
        }
        return list.iterator();
    }

    @Override
    public boolean isInternal(Node<E> node) throws IllegalArgumentException {
        if (node == null) {
            throw new IllegalArgumentException();
        }
        return childrenNumber(node) > 0;
    }

    @Override
    public boolean isExternal(Node<E> node) throws IllegalArgumentException {
        if (node == null) {
            throw new IllegalArgumentException();
        }
        return childrenNumber(node) == 0;
    }

    @Override
    public boolean isRoot(Node<E> node) throws IllegalArgumentException {
        return node.equals(root());
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public Collection<Node<E>> nodes() {
        return breadthFirst();
    }

    @Override
    public Collection<Node<E>> preOrder() {
        List<Node<E>> list = new ArrayList<>();
        Stack<Node<E>> stack = new Stack<>();
        stack.push(root());
        while (!stack.isEmpty()) {
            Node<E> n = stack.pop();
            list.add(n);
            if (left(n) != null) {
                stack.push(left(n));
            }
            if (right(n) != null) {
                stack.push(right(n));
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> inOrder() {
        List<Node<E>> list = new ArrayList<>();
        Stack<Node<E>> stack = new Stack<>();
        Node<E> currentNode = root();

        while (!stack.isEmpty() || currentNode != null) {
            if (currentNode != null) {
                stack.push(currentNode);
                currentNode = left(currentNode);
            } else {
                Node<E> n = stack.pop();
                list.add(n);
                currentNode = right(n);
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        List<Node<E>> list = new ArrayList<>();
        Stack<Node<E>> stack = new Stack<>();
        Node<E> current = root();

        while (true) {
            if (current != null) {
                if (right(current) != null) {
                    stack.push(right(current));
                }
                stack.push(current);
                current = left(current);
                continue;
            }
            if (stack.isEmpty()) {
                break;
            }
            current = stack.pop();

            if (right(current) != null && !stack.isEmpty() && right(current) == stack.peek()) {
                stack.pop();
                stack.push(current);
                current = right(current);
            } else {
                list.add(current);
                current = null;
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        List<Node<E>> list = new ArrayList<>();
        Queue<Node<E>> queue = new LinkedList<>();
        queue.add(root());

        while (!queue.isEmpty()) {
            Node<E> n = queue.poll();
            list.add(n);
            if (left(n) != null) {
                queue.add(left(n));
            }
            if (right(n) != null) {
                queue.add(right(n));
            }
        }
        return list;
    }

    @Override
    public String toString() {
        return toString(root());
    }

    private String toString(Node<E> root) {
        StringBuilder sb = new StringBuilder();
        Collection<Node<E>> collection = breadthFirst();
        for (Node<E> node : collection) {
            sb.append(node.getElement()).append(" ");
        }
        return sb.toString();
    }

    public static class NodeImpl<E> implements Node<E> {

        private NodeImpl<E> left;
        private NodeImpl<E> right;
        private NodeImpl<E> parent;
        private E element;
        private boolean isBlack;

        NodeImpl(NodeImpl<E> left, E element, NodeImpl<E> right, NodeImpl<E> parent) {
            this.left = left;
            this.right = right;
            this.element = element;
            this.parent = parent;
        }

        public NodeImpl() {
        }

        @Override
        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public NodeImpl<E> getLeft() {
            return this.left;
        }

        public void setLeft(Node<E> left) {
            this.left = (NodeImpl<E>) left;
        }

        public NodeImpl<E> getRight() {
            return this.right;
        }

        public void setRight(Node<E> right) {
            this.right = (NodeImpl<E>) right;
        }

        public NodeImpl<E> getParent() {
            return parent;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public boolean getBlack() {
            return isBlack;
        }

        public void setBlack(boolean black) {
            isBlack = black;
        }

    }
}