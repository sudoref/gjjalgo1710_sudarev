package com.getjavajob.training.algo1710.sudarevr.lesson05;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeTest {
    public static void main(String[] args) {
        dequeTestAdd();
        dequeTestAddFirst();
        dequeTestAddLast();
        dequePop();
        dequePush();
        dequeRemoveFirstOccurrence();
        dequeRemoveLastOccurrence();
        dequeTestGetFirst();
        dequeTestGetLast();
        dequeTestOfferFirst();
        dequeTestOfferLast();
        dequeTestPeekFirst();
        dequeTestPeekLast();
        dequeTestPollFirst();
        dequeTestPollLast();
        dequeTestRemoveFirst();
        dequeTestRemoveLast();
    }

    public static void dequeTestAdd() {
        Deque<String> deque = new ArrayDeque<>();
        Assert.assertEquals("DequeTest.dequeTestAdd()", true, deque.add("1"));
    }

    public static void dequeTestAddFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.addFirst("1");
        Assert.assertEquals("DequeTest.dequeTestAddFirst()", "1", deque.getFirst());
    }

    public static void dequeTestAddLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("2");
        deque.addLast("1");
        Assert.assertEquals("DequeTest.dequeTestAddLast()", "1", deque.getLast());
    }

    public static void dequeTestOfferFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestOfferFirst()", true, deque.offerFirst("1"));
    }

    public static void dequeTestOfferLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestOfferLast()", true, deque.offerLast("1"));
    }

    public static void dequeTestRemoveFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestRemoveFirst()", "0", deque.removeFirst());
    }

    public static void dequeTestRemoveLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestRemoveLast()", "1", deque.removeLast());
    }

    public static void dequeTestPollFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestPollFirst()", "0", deque.pollFirst());
    }

    public static void dequeTestPollLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestPollLast()", "1", deque.pollLast());
    }

    public static void dequeTestGetFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestGetFirst()", "0", deque.getFirst());
    }

    public static void dequeTestGetLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestGetLast()", "1", deque.getLast());
    }

    public static void dequeTestPeekFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestPeekFirst()", "0", deque.peekFirst());
    }

    public static void dequeTestPeekLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeTestPeekLast()", "1", deque.peekLast());
    }

    public static void dequeRemoveFirstOccurrence() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeRemoveFirstOccurrence()", true, deque.removeFirstOccurrence("0"));
    }

    public static void dequeRemoveLastOccurrence() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        Assert.assertEquals("DequeTest.dequeRemoveLastOccurrence()", true, deque.removeLastOccurrence("1"));
    }

    public static void dequePush() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        deque.push("2");
        Assert.assertEquals("DequeTest.dequeTestPush()", "1", deque.getLast());
    }

    public static void dequePop() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("0");
        deque.add("1");
        deque.pop();
        Assert.assertEquals("DequeTest.dequeTestPeekLast()", "1", deque.getFirst());
    }
}
