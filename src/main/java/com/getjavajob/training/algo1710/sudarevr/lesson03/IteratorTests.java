package com.getjavajob.training.algo1710.sudarevr.lesson03;

import com.getjavajob.training.algo1710.util.Assert;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class IteratorTests {
    public static void main(String[] args) {
        testIteratorNext();
        testIteratorHasNext();
        testHasPrevious();
        testPrevious();
        testNextIndex();
        testPreviousIndex();
        testRemove();
        testSet();
        testAdd();
    }

    static void testIteratorNext() {
        DynamicArray da = new DynamicArray();
        da.add("next");
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        assertEquals("IteratorTests.testIteratorNext()", "next", (String) iterator.next());
    }

    static void testIteratorHasNext() {
        DynamicArray da = new DynamicArray();
        da.add("next");
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        DynamicArray.ListIteratorImpl iterator1 = da.listIterator();
        assertEquals("IteratorTests.testIteratorHasNext(true)", true, iterator.hasNext());
    }

    static void testHasPrevious() {
        DynamicArray da = new DynamicArray();
        da.add("next");
        da.add("next");
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        DynamicArray.ListIteratorImpl iterator1 = da.listIterator();
        iterator.next();
        iterator.next();
        assertEquals("IteratorTests.testHasPrevious(true)", true, iterator.hasPrevious());
        assertEquals("IteratorTests.testHasPrevious(false)", false, iterator1.hasPrevious());
    }

    static void testPrevious() {
        DynamicArray da = new DynamicArray();
        da.add("next");
        da.add("next");
        DynamicArray.ListIteratorImpl listIterator = da.listIterator();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIteratorImpl.testListIteratorPrevious", "next", (String) listIterator.previous());
    }

    static void testNextIndex() {
        DynamicArray da = new DynamicArray(10);
        da.add("next");
        da.add("next");
        da.add("next");
        da.add("next");
        DynamicArray.ListIteratorImpl listIterator = da.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIteratorImpl.testListIteratorNextIndex", 3, listIterator.nextIndex());
    }

    static void testPreviousIndex() {
        DynamicArray da = new DynamicArray();
        da.add("previous");
        da.add(2);
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        assertEquals("IteratorTests.testPreviousIndex", 0, iterator.previousIndex());
    }

    static void testRemove() {
        DynamicArray da = new DynamicArray();
        da.add("first");
        da.add(2);
        da.add(3);
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        while (iterator.hasNext()) {
            Object e = iterator.next();
            if (e.equals("first")) {
                iterator.remove();
            }
        }
        Object[] arr = da.toArray();
        Object[] arr2 = {2, 3};
        assertEquals("IteratorTests.testRemoveList", arr2, arr);
    }

    static void testSet() {
        DynamicArray da = new DynamicArray();
        da.add("first");
        da.add(2);
        da.add(3);
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        while (iterator.hasNext()) {
            Object e = iterator.next();
            if (e.equals("first")) {
                iterator.set("second");
            }
        }
        assertEquals("IteratorTests.testSet", "second", (String) da.get(0));
    }

    static void testAdd() {
        DynamicArray da = new DynamicArray();
        da.add("first");
        da.add(2);
        da.add(3);
        DynamicArray.ListIteratorImpl iterator = da.listIterator();
        iterator.add("obj");
        assertEquals("IteratorTests.testAdd", 3, (Integer) da.get(3));
    }
}
