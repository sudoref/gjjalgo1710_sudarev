package com.getjavajob.training.algo1710.sudarevr.lesson06;

import static java.lang.Math.abs;

public class AssociativeArray<K, V> {
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private int size;
    private int threshold;
    private Node<K, V>[] table;
    public AssociativeArray() {
        table = new Node[DEFAULT_INITIAL_CAPACITY];
    }

    private static int indexFor(int hash, int length) {
        return abs(hash % length);
    }

    public int size() {
        return size;
    }

    public V add(K key, V value) {
        int hash = getHash(key);
        int i = indexFor(hash, table.length);
        for (Node<K, V> node = table[i]; node != null; node = node.next) {
            if (node.hash == hash && node.key == key || key.equals(node.key)) {
                V oldValue = node.value;
                node.value = value;
                return oldValue;
            }
        }

        addNode(hash, key, value, i);
        return null;
    }

    private int getHash(K key){
        return (key.hashCode() & 0x7fffffff) % table.length;
    }

    public V remove(K key) {
        int hash = getHash(key);
        int i = indexFor(hash, table.length);
        Node<K, V> prev = table[i];
        Node<K, V> e = prev;

        while (e != null) {
            Node<K, V> next = e.next;
            if (e.hash == hash && (e.key == key || key.equals(e.key))) {
                size--;
                if (prev == e) {
                    table[i] = next;
                } else {
                    prev.next = next;
                }
                return e.value;
            }
            prev = e;
            e = next;
        }
        return null;
    }

    public V get(K key) {
        int hash = getHash(key);
        for (Node<K, V> e = table[indexFor(hash, table.length)];
             e != null;
             e = e.next) {
            if (e.value == null) {
                throw new NullPointerException("No key");
            }
            if (e.hash == hash && (e.key == key || key.equals(e.key))) {
                return e.value;
            }
        }
        return null;
    }

    private void addNode(int hash, K key, V value, int bucketIndex) {
        Node<K, V> node = table[bucketIndex];
        table[bucketIndex] = new Node<>(hash, key, value, node);
        if (size++ >= threshold) {
            resize(2 * table.length);
        }
    }

    private void resize(int newCapacity) {
        Node[] newTable = new Node[newCapacity];
        transfer(newTable);
        table = newTable;
        threshold = (int) (newCapacity * 0.75);
    }

    private void transfer(Node[] newTable) {
        Node[] src = table;
        int newCapacity = newTable.length;
        for (int j = 0; j < src.length; j++) {
            Node<K, V> e = src[j];
            if (e != null) {
                src[j] = null;
                do {
                    Node<K, V> next = e.next;
                    int i = indexFor(e.hash, newCapacity);
                    e.next = newTable[i];
                    newTable[i] = e;
                    e = next;
                } while (e != null);
            }
        }
    }

    static class Node<K, V> {
        final K key;
        final int hash;
        V value;
        Node<K, V> next;

        public Node(int hash, K key, V value, Node<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
            this.hash = hash;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }
    }
}
