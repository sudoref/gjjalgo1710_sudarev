package com.getjavajob.training.algo1710.sudarevr.lesson06;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SetTest {
    public static void main(String[] args) {
        setAddTest();
        setAddAllTest();
    }

    private static void setAddTest() {
        Set<String> set = new HashSet<>();
        set.add("test1");
        set.add("test1");
        set.add("test2");
        set.add("test2");

        Assert.assertEquals("setAddTest", 2, set.size());
    }

    private static void setAddAllTest() {
        Map<String, Integer> map = new HashMap<>();
        map.put("1", 1);
        map.put("2", 2);
        map.put("3", 2);
        Set<Integer> set = new HashSet<>();
        set.addAll(map.values());
        Integer[] arr = set.toArray(new Integer[set.size()]);
        Integer[] arr1 = {1, 2};

        Assert.assertEquals("setAddAllTest", arr1, arr);
    }
}
