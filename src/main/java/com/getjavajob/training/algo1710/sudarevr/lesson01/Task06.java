package com.getjavajob.training.algo1710.sudarevr.lesson01;

public class Task06 {

    /**
     * given n<31, calc 2^n
     */
    public static int getPowerOfNumber(int num, int pow) {
        if (pow >= 0 && pow < 31) {
            return num << pow;
        } else {
            return 0;
        }
    }

    /**
     * given n,m<31, calc 2^n+2^m
     */
    public static int getSum(int num01, int pow01, int num02, int pow02) {
        return getPowerOfNumber(num01, pow01) | getPowerOfNumber(num02, pow02);
    }

    /**
     * reset n lower bits (create mask with n lower bits reset)
     */
    public static int resetNLowBits(int num, int bits) {
        return num & ~createMask(bits);
    }

    public static int createMask(int bits) {
        int result = 0;
        for (int i = 0; i <= bits; i++) {
            result |= 1 << i;
        }
        return result;
    }

    /**
     * set a's n-th bit with 1
     */
    public static int setNbitWithOne(int num, int bit) {
        return num | (1 << bit);
    }

    /**
     * invert n-th bit (use 2 bit ops)
     */
    public static int invertBit1(int num, int bit) {
        return num & ~(1 << bit) | ~num & (1 << bit);
    }

    public static int invertBit2(int num, int bit) {
        return num ^ (1 << bit);
    }

    /**
     * set a's n-th bit with 0
     */
    public static int setBitWithZero(int num, int bit) {
        return num & ~(1 << bit);
    }

    /**
     * return n lower bits
     */
    public static int getNLowerBits(int num, int bit) {
        int mask = 0;
        for (int i = 0; i <= bit; i++) {
            mask |= 1 << i;
        }
        return num & mask;
    }

    /**
     * return n-th bit
     */
    public static int getNBit(int num, int bit) {
        return num >> bit & 1;
    }

    /**
     * given byte a. output bin representation using bit ops (don't use jdk api)
     */
    public static String representBin(int num) {
        StringBuilder sb = new StringBuilder();
        sb.append(num & 1);
        while ((num >>= 1) != 0) {
            sb.append(num & 1);
        }
        return sb.reverse().toString();
    }
}
