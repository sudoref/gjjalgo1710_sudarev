package com.getjavajob.training.algo1710.sudarevr.lesson09.UnitTests;

import java.util.SortedMap;
import java.util.TreeMap;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class SortedMapTest {
    public static void main(String[] args) {
        testSubMap();
        testHeadMap();
        testTailMap();
        testFirstKey();
        testLastKey();
        testKeySet();
        testValues();
        testEntrySet();
    }

    private static void testEntrySet() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");
        sortedMap.put(3, "three");


        assertEquals("testEnrytSet", "[0=zero, 1=one, 2=two, 3=three]", sortedMap.entrySet().toString());
    }

    private static void testValues() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");

        assertEquals("testValues", "[zero, one, two]", sortedMap.values().toString());
    }

    private static void testKeySet() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");
        sortedMap.put(3, "three");
        sortedMap.put(4, "four");
        sortedMap.put(5, "five");
        sortedMap.put(6, "six");
        sortedMap.put(7, "seven");
        sortedMap.put(8, "eight");
        sortedMap.put(9, "nine");

        assertEquals("testKeySet", "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", sortedMap.keySet().toString());
    }

    private static void testLastKey() {
        SortedMap<String, Integer> sortedMap = new TreeMap<>();
        sortedMap.put("zero", 0);
        sortedMap.put("one", 1);
        sortedMap.put("two", 2);

        assertEquals("testLastKey", "zero", sortedMap.lastKey());
    }

    private static void testFirstKey() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");
        sortedMap.put(3, "three");
        sortedMap.put(4, "four");
        sortedMap.put(5, "five");
        sortedMap.put(6, "six");
        sortedMap.put(7, "seven");
        sortedMap.put(8, "eight");
        sortedMap.put(9, "nine");

        assertEquals("testFirstKey", 0, sortedMap.firstKey());
    }

    private static void testTailMap() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");
        sortedMap.put(3, "three");
        sortedMap.put(4, "four");
        sortedMap.put(5, "five");
        sortedMap.put(6, "six");
        sortedMap.put(7, "seven");
        sortedMap.put(8, "eight");
        sortedMap.put(9, "nine");

        assertEquals("testTailMap", "{6=six, 7=seven, 8=eight, 9=nine}", sortedMap.tailMap(6).toString());
    }

    private static void testHeadMap() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");
        sortedMap.put(3, "three");
        sortedMap.put(4, "four");
        sortedMap.put(5, "five");
        sortedMap.put(6, "six");
        sortedMap.put(7, "seven");
        sortedMap.put(8, "eight");
        sortedMap.put(9, "nine");

        assertEquals("testHeadMap", "{0=zero, 1=one, 2=two}", sortedMap.headMap(3).toString());
    }

    private static void testSubMap() {
        SortedMap<Integer, String> sortedMap = new TreeMap<>();
        sortedMap.put(0, "zero");
        sortedMap.put(1, "one");
        sortedMap.put(2, "two");
        sortedMap.put(3, "three");
        sortedMap.put(4, "four");
        sortedMap.put(5, "five");
        sortedMap.put(6, "six");
        sortedMap.put(7, "seven");
        sortedMap.put(8, "eight");
        sortedMap.put(9, "nine");

        assertEquals("testSubMap", "{7=seven, 8=eight}", sortedMap.subMap(7, 9).toString());
    }


}
