package com.getjavajob.training.algo1710.sudarevr.lesson06;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Set;

public class LinkedHashMapTest {
    public static void main(String[] args) {
        linkedHashMapAdd();
        linkedHashMapAddAll();
    }

    private static void linkedHashMapAdd() {
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("beta", 1);
        linkedHashMap.put("Alpha", 1);
        linkedHashMap.put("Eta", 2);
        linkedHashMap.put("Gamma", 2);
        linkedHashMap.put("Epsilon", 3);
        linkedHashMap.put("Omega", 3);

        Assert.assertEquals("linkedHashMapAdd", "{beta=1, Alpha=1, Eta=2, Gamma=2, Epsilon=3, Omega=3}", linkedHashMap.toString());
    }

    private static void linkedHashMapAddAll() {
        Set<String> set = Collections.newSetFromMap(new LinkedHashMap<>());
        set.add("one");
        set.add("two");
        set.add("three");
        set.add("two");

        Assert.assertEquals("linkedHashMapAddAll", "[one, two, three]", set.toString());
    }
}

