package com.getjavajob.training.algo1710.sudarevr.lesson10;

public class BubbleSort {

    public static int[] sort(int[] inputArray) {
        if (isSorted(inputArray))return inputArray;
        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i] > inputArray[i + 1]) {
                int temp = inputArray[i];
                inputArray[i] = inputArray[i + 1];
                inputArray[i + 1] = temp;
                sort(inputArray);
            }
        }
        return inputArray;
    }

     static boolean isSorted(int[] a){
        if(a == null) {
            return false;
        }
        else if(a.length == 0) {
            return true;
        }
        for (int i = 0; i < a.length-1; i++) {
            if(a[i] > a[i+1]) {
                return false;
            }
        }
        return true;
    }
}
