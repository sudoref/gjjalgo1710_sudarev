package com.getjavajob.training.algo1710.sudarevr.lesson09.RedBlackTree;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;
import com.getjavajob.training.algo1710.sudarevr.lesson08.balanced.BalanceableTree;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E> extends BalanceableTree<E> {

    boolean isBlack(Node<E> n) {
        return validate(n).getBlack();
    }

    boolean isRed(Node<E> n) {
        return !isBlack(n);
    }

    private void makeBlack(Node<E> n) {
        ((NodeImpl<E>) n).setBlack(true);
    }

    private void makeRed(Node<E> n) {
        ((NodeImpl<E>) n).setBlack(false);
    }

    private NodeImpl<E> getGrandParent(Node<E> n) {
        NodeImpl<E> node = validate(n);
        if (node.getParent() == null || node.getParent().getParent() == null) {
            return null;
        }
        return node.getParent().getParent();
    }

    private NodeImpl<E> getUncle(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (getGrandParent(n) == null) {
            return null;
        }
        if (getGrandParent(node).getLeft() != null && getGrandParent(node).getLeft() == node.getParent()) {
            return getGrandParent(node).getRight();
        } else if (getGrandParent(node).getRight() != null && getGrandParent(node).getRight() == node.getParent()) {
            return getGrandParent(node).getLeft();
        } else {
            throw new IllegalArgumentException();
        }
    }

    private NodeImpl<E> getSibling(Node<E> n) {
        NodeImpl<E> node = validate(n);
        if (node.getParent().getLeft().equals(n)) {
            return node.getParent().getRight();
        } else {
            return node.getParent().getLeft();
        }
    }

    @Override
    public Node<E> add(Node<E> node, E element) throws IllegalArgumentException {
        Node<E> newNode = node;

        if (compare(element, node.getElement()) > 0) {
            if (right(node) == null) {
                newNode = addRight(node, element);
            } else {
                newNode = add(right(node), element);
            }
        } else if (compare(element, node.getElement()) < 0) {
            if (left(node) == null) {
                newNode = addLeft(node, element);
            } else {
                newNode = add(left(node), element);
            }
        }
        afterElementAdded(newNode);
        return newNode;
    }

    @Override
    protected void afterElementAdded(Node<E> node) {
        if (isRoot(node)) {
            // If x is root, change color of x as BLACK
            makeBlack(node);
        } else if (isRed(parent(node))) {
            insertCaseOne(validate(node));
        }
    }

    private void insertCaseOne(NodeImpl<E> node) {
        /*
            If x’s uncle is RED (Grand parent must have been black from property 4
            1 Change color of parent and uncle as BLACK.
            2 color of grand parent as RED.
            3 Change x = x’s grandparent, repeat steps new x.
            */
        NodeImpl<E> uncle = getUncle(node);
        if (uncle != null && !uncle.getBlack()) {
            makeBlack(uncle);
            makeBlack(node.getParent());
            NodeImpl<E> grandparent = getGrandParent(node);
            makeRed(grandparent);
            afterElementAdded(grandparent);
        } else {
            insertCaseTwo(node);
        }
    }

    private void insertCaseTwo(NodeImpl<E> node) {
        /*
        1 Left Left Case (p is left child of g and x is left child of p)
        2 Left Right Case (p is left child of g and x is right child of p)
        3 Right Right Case (Mirror of case a)
        4 Right Left Case (Mirror of case c)
        */
        Node<E> grandparent = parent(parent(node));
        if (node.equals(node.getParent().getRight()) && node.getParent().equals(left(grandparent))) {
            rotate(node);
            node = node.getLeft();
        } else if (node.equals(node.getParent().getLeft()) && node.getParent().equals(right(grandparent))) {
            rotate(node);
            node = node.getRight();
        }
        makeBlack(node.getParent());
        makeRed(grandparent);
        rotate(node.getParent());
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int size = size();
        if (size == 0) {
            throw new IllegalStateException("IllegalStateException");
        }
        int childrenNumber = childrenNumber(n);
        E returnElement = n.getElement();

        NodeImpl<E> tempNode = new NodeImpl<>();
        tempNode.setParent(validate(n).getParent());
        tempNode.setRight(validate(n).getRight());
        tempNode.setLeft(validate(n).getLeft());
        tempNode.setElement(validate(n).getElement());
        boolean isNeedActionAfterRemove = true;
        if (!tempNode.getBlack() && childrenNumber(tempNode) == 2 && (tempNode.getRight().getLeft() != null ||
                tempNode.getLeft().getRight() != null)) {
            isNeedActionAfterRemove = false;
        }
        if (childrenNumber == 0) {
            removeLeaf(n);
        } else if (childrenNumber(n) == 1) {
            removeOneChild(n);
        } else {
            NodeImpl<E> node = validate(n);
            NodeImpl<E> checkNode = node.getLeft();
            if (node.getRight().getLeft() != null) {
                checkNode = returnLeft(node.getRight());
            } else if (node.getLeft().getRight() != null) {
                checkNode = returnRight(node.getLeft());
            }
            if (checkNode.getRight() == null) {
                node.setElement(checkNode.getElement());
                removeLeaf(checkNode);
            } else {
                node.setElement(checkNode.getElement());
                removeOneChild(checkNode);
            }
        }
        --size;
        if (isNeedActionAfterRemove) {
            afterElementRemoved(tempNode);
        }
        return returnElement;
    }

    private NodeImpl<E> returnLeft(NodeImpl<E> n) {
        NodeImpl<E> leftNode = n;
        while (leftNode.getLeft() != null) {
            leftNode = leftNode.getLeft();
        }
        return leftNode;
    }

    private NodeImpl<E> returnRight(NodeImpl<E> n) {
        NodeImpl<E> rightNode = n;
        while (rightNode.getRight() != null) {
            rightNode = rightNode.getRight();
        }
        return rightNode;
    }

    private void removeLeaf(Node<E> n) {
        Node<E> parent = parent(n);
        if (left(parent) == n) {
            validate(parent).setLeft(null);
        } else {
            validate(parent).setRight(null);
        }
    }

    private void removeOneChild(Node<E> n) {
        NodeImpl<E> node = validate(n);
        NodeImpl<E> newRoot = node.getLeft() != null ? node.getLeft() : node.getRight();
        if (!isRoot(n)) {
            NodeImpl<E> parent = validate(parent(n));
            if (parent.getLeft() != null && parent.getLeft().equals(n)) {
                parent.setLeft(newRoot);
            } else {
                parent.setRight(newRoot);
            }
        } else {
            addRoot(newRoot);
        }
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        NodeImpl<E> currentNode = validate(n);
        if (!currentNode.getBlack() && currentNode.getRight() != null && currentNode.getLeft() != null) {
            makeBlack(currentNode.getLeft().getParent());
            makeRed(currentNode.getRight());
        } else if (isBlack(currentNode)) {
            if (childrenNumber(currentNode) == 1) {
                if (currentNode.getRight() != null) {
                    makeBlack(currentNode.getRight());
                } else {
                    makeBlack(currentNode.getLeft());
                }
            } else if (isExternal(currentNode)) {
                NodeImpl<E> sibling = getSibling(currentNode);
                if (isRed(sibling)) {
                    //If sibling is red, perform a rotation to move old sibling up, recolor the old sibling and parent.
                    if (sibling != null && sibling.getLeft() != null) {
                        makeRed(sibling.getLeft());
                    }
                    rotate(sibling);
                } else if (isExternal(sibling)) {
                    /* Color u as double black.
                     Now our task reduces to convert this double black to single black.
                     Note that If v is leaf, then u is NULL and color of NULL is considered as black.
                     So the deletion of a black leaf also causes a double black.
                    */
                    makeRed(sibling);
                    makeBlack(parent(currentNode));
                } else if (sibling != null) {
                    if (childrenNumber(sibling) == 2 &&
                            isRed(sibling.getLeft()) && isBlack(sibling.getRight())) {
                        rotate(sibling.getLeft());
                        makeBlack(sibling);
                        makeRed(sibling.getRight());
                    } else if (childrenNumber(sibling) == 2 && isRed(sibling.getRight())) {
                        //If sibling s is black and at least one of sibling’s children is red, perform rotation(s).
                        makeBlack(sibling.getRight());
                        rotate(sibling);
                        makeBlack(currentNode.getParent());
                    }
                }
            }
        }
    }
}
