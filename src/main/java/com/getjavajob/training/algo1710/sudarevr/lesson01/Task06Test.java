package com.getjavajob.training.algo1710.sudarevr.lesson01;

import static com.getjavajob.training.algo1710.sudarevr.lesson01.Task06.*;
import static com.getjavajob.training.algo1710.util.Assert.assertEquals;
import static com.getjavajob.training.algo1710.util.BinRepresent.convertNumber;

public class Task06Test {
    public static void main(String[] args) {
        testGetPowerOfNumber();
        testGetSum();
        testResetNLowBits();
        testSetNbitWithOne();
        testInvertBit1();
        testInvertBit2();
        testSetBitWithZero();
        testGetNLowerBits();
        testGetNBit();
        testRepresentBin();
    }

    private static void testGetPowerOfNumber() {
        assertEquals("Task06.testGetPowerOfNumber", "1000", convertNumber(getPowerOfNumber(2, 2)));
    }

    private static void testGetSum() {
        assertEquals("Task06.testGetSum", "101000", convertNumber(getSum(2, 2, 2, 4)));
    }

    private static void testResetNLowBits() {
        assertEquals("Task06.testResetNLowBits", "11101000", convertNumber(resetNLowBits(237, 2)));
    }

    private static void testSetNbitWithOne() {
        assertEquals("Task06.testSetNbitWithOne", "11111101", convertNumber(setNbitWithOne(237, 4)));
    }

    private static void testInvertBit1() {
        assertEquals("Task06.testInvertBit1", "11101001", convertNumber(invertBit1(237, 2)));
    }

    private static void testInvertBit2() {
        assertEquals("Task06.testInvertBit2", "11101001", convertNumber(invertBit1(237, 2)));
    }

    private static void testSetBitWithZero() {
        assertEquals("Task06.testSetBitWithZero", "11101100", convertNumber(setBitWithZero(237, 0)));
    }

    private static void testGetNLowerBits() {
        assertEquals("Task06.testGetNLowerBits", "101", convertNumber(getNLowerBits(237, 2)));
    }

    private static void testGetNBit() {
        assertEquals("Task06.testGetNBit", "0", convertNumber(getNBit(237, 4)));
    }

    private static void testRepresentBin() {
        assertEquals("Task06.testRepresentBin", "11101101", representBin(237));
    }
}
