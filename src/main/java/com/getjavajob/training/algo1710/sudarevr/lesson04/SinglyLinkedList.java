package com.getjavajob.training.algo1710.sudarevr.lesson04;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<E> {

    private Node<E> head;
    private int size;

    void add(E e) {
        Node<E> h = head;
        Node<E> newNode = new Node<>(e, null);
        if (head == null) {
            head = newNode;
        } else {
            while (h.next != null) {
                h = h.next;
            }
            h.next = newNode;
        }
        size++;
    }

    E get(int index) {
        Node<E> x = head;
        for (int i = 0; i < index; i++) {
            x = x.next;
        }
        return x.val;
    }

    int size() {
        return size;
    }

    void reverse() {
        Node<E> currentElement = head;
        Node<E> reversedElement = null;
        while (currentElement != null) {
            Node nextElement = currentElement.next;
            currentElement.next = reversedElement;
            reversedElement = currentElement;
            currentElement = nextElement;
        }
        head = reversedElement;
    }

    void relink(int index1, int index2) {
        Node<E> node1 = head;
        for (int i = 0; i < index1; i++) {
            node1 = node1.next;
        }

        Node<E> node2 = head;
        for (int i = 0; i < index2; i++) {
            node2 = node2.next;
        }

        E data = node1.val;
        node1.val = node2.val;
        node2.val = data;
    }

    List<E> asList() {
        List<E> list = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            list.add(get(i));
        }
        return list;
    }

    static class Node<E> {
        Node<E> next;
        E val;

        public Node(E val, Node<E> next) {
            this.next = next;
            this.val = val;
        }
    }
}

