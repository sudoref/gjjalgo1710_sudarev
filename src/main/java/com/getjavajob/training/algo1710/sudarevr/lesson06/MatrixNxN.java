package com.getjavajob.training.algo1710.sudarevr.lesson06;

import java.util.HashMap;
import java.util.Map;

public class MatrixNxN<V> implements Matrix<V> {

    private Map<Key, V> map;
    private int size;

    public MatrixNxN() {
        this.map = new HashMap<>();
    }

    @Override
    public V get(int i, int j) {
        return map.get(new Key(i, j));
    }

    @Override
    public void set(int i, int j, V value) {
        this.map.put(new Key(i, j), value);
        size++;
    }

    public int size() {
        return size;
    }
}

class Key {

    private final int i;
    private final int j;

    public Key(int i, int j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Key)) {
            return false;
        }
        Key key = (Key) o;
        return i == key.i && j == key.j;
    }

    @Override
    public int hashCode() {
        int result = i;
        result = 31 * result + j;
        return result;
    }
}