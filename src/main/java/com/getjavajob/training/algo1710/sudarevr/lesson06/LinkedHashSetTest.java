package com.getjavajob.training.algo1710.sudarevr.lesson06;

import com.getjavajob.training.algo1710.util.Assert;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class LinkedHashSetTest {
    public static void main(String[] args) {
        linkedHashSetAdd();
        linkedHashSetAddAll();
    }

    private static void linkedHashSetAdd() {
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add("beta");
        linkedHashSet.add("Alpha");
        linkedHashSet.add("Eta");
        linkedHashSet.add("Gamma");
        linkedHashSet.add("Epsilon");
        linkedHashSet.add("Omega");
        Assert.assertEquals("linkedHashSetAdd", "[beta, Alpha, Eta, Gamma, Epsilon, Omega]", linkedHashSet.toString());
    }

    private static void linkedHashSetAddAll() {
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("beta", 1);
        linkedHashMap.put("Alpha", 1);
        linkedHashMap.put("Eta", 2);
        linkedHashMap.put("Gamma", 2);
        linkedHashMap.put("Epsilon", 3);
        linkedHashMap.put("Omega", 3);

        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.addAll(linkedHashMap.values());

        Assert.assertEquals("linkedHashSetAddAll", "[1, 2, 3]", linkedHashSet.toString());
    }
}
