package com.getjavajob.training.algo1710.sudarevr.lesson10;

public class QuickSort {
    public static Comparable[] sort(Comparable[] a) {
        return sort(a, 0, a.length - 1);
    }

    private static Comparable[] sort(Comparable[] a, int lo, int hi) {
        if (lo >= hi) return a;
        int j = partition(a, lo, hi);
        sort(a, lo, j - 1);
        sort(a, j + 1, hi);
        return a;
    }

    private static int partition(Comparable[] a, int lo, int hi) {
        int i = lo;
        int j = hi + 1;
        Comparable part = a[lo];

        while (true) {
            while (a[++i].compareTo(part) < 0) {
                if (i == hi) break;
            }
            while (part.compareTo(a[--j]) < 0) {
                if (j == lo) break;
            }
            if (i >= j) break;
            exch(a, i, j);
        }
        exch(a, lo, j);
        return j;
    }


    private static void exch(Comparable[] a, int i, int j) {
        Comparable swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

}
