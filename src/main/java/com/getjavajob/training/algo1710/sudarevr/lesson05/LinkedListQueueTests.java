package com.getjavajob.training.algo1710.sudarevr.lesson05;

import com.getjavajob.training.algo1710.util.Assert;

public class LinkedListQueueTests {
    public static void main(String[] args) {
        testAdd();
        testRemove();
        testSize();
        testAsList();
    }

    public static void testAdd() {
        LinkedListQueue<String> llq = new LinkedListQueue<>();
        Assert.assertEquals("LinkedListQueueTests.testAdd", true, llq.add("test"));
    }

    public static void testRemove() {
        LinkedListQueue<String> llq = new LinkedListQueue<>();
        llq.add("1");
        llq.add("2");
        Assert.assertEquals("LinkedListQueueTests.testRemove", "1", llq.remove());
    }

    public static void testSize() {
        LinkedListQueue<String> llq = new LinkedListQueue<>();
        llq.add("1");
        llq.add("2");
        Assert.assertEquals("LinkedListQueueTests.testSize", 2, llq.size());
    }

    public static void testAsList() {
        LinkedListQueue<String> llq = new LinkedListQueue<>();
        llq.add("1");
        llq.add("2");
        String[] arr = {"1", "2"};

        Assert.assertEquals("LinkedListQueueTests.testAsList", arr, llq.asList().toArray());
    }
}
