package com.getjavajob.training.algo1710.sudarevr.lesson04;

import com.getjavajob.training.algo1710.util.Assert;

public class DoublyLinkedListTests {
    public static void main(String[] args) {
        testBeginAdd();
        testMiddleAdd();
        testEndAdd();
        testBeginRemove();
        testMiddleRemove();
        testEndRemove();
        testRemoveObj();
        testGet();
        testException();
    }

    private static void testBeginAdd() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        Assert.assertEquals("DoublyLinkedListTests.testBeginAdd()", "test", dll.get(0));
    }

    private static void testMiddleAdd() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        dll.add("test3");
        dll.add(1, "test2");
        Assert.assertEquals("DoublyLinkedListTests.testMiddleAdd()", "test2", dll.get(1));
    }

    private static void testEndAdd() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        dll.add("test3");
        dll.add(1, "test2");
        Assert.assertEquals("DoublyLinkedListTests.testEndAdd", "test3", dll.get(2));
    }

    private static void testBeginRemove() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        dll.add(1, "test2");
        dll.add("test3");
        dll.remove(0);
        Assert.assertEquals("DoublyLinkedListTests.testBeginRemove", "test2", dll.get(0));
    }

    private static void testMiddleRemove() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        dll.add(1, "test2");
        dll.add("test3");
        dll.remove(1);
        Assert.assertEquals("DoublyLinkedListTests.testMiddleRemove", "test3", dll.get(1));
    }

    private static void testEndRemove() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        dll.add(1, "test2");
        dll.add("test3");
        dll.remove(2);
        Object[] arr = dll.toArray();
        Object[] testArr = {"test", "test2"};
        Assert.assertEquals("DoublyLinkedListTests.testEndRemove", testArr, arr);
    }

    private static void testRemoveObj() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add(0, "test");
        dll.add(1, "test2");
        dll.add("test3");
        dll.remove("test3");
        Object[] arr = dll.toArray();
        Object[] testArr = {"test", "test2"};
        Assert.assertEquals("DoublyLinkedListTests.testRemoveObj", testArr, arr);
    }

    private static void testGet() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add("test");
        Assert.assertEquals("DoublyLinkedListTests.testGet", "test", dll.get(0));
    }

    private static void testException() {
        try {
            DoublyLinkedList<String> dll = new DoublyLinkedList<>();
            dll.add(-1, "test");
            Assert.fail("DoublyLinkedListTests.testExceptionAdd() failed");
        } catch (Exception e) {
            Assert.assertEquals("DoublyLinkedListTests.testExceptionAdd()", "index is negative", e.getMessage());
        }

        try {
            DoublyLinkedList<String> dll = new DoublyLinkedList<>();
            dll.get(10);
            Assert.fail("DoublyLinkedListTests.testExceptionGet() failed");
        } catch (Exception e) {
            Assert.assertEquals("DoublyLinkedListTests.testExceptionGet()", "Index: 10, Size: 0", e.getMessage());
        }
    }

}
