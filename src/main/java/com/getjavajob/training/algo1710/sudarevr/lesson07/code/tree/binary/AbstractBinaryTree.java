package com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.AbstractTree;
import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parentNode = parent(n);
        if (left(parentNode).equals(n)) {
            return right(parentNode);
        } else if (right(parentNode).equals(n)) {
            return left(parentNode);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        List<Node<E>> nodeList = new ArrayList<>();
        if (left(n) != null) {
            nodeList.add(left(n));
        }
        if (right(n) != null) {
            nodeList.add(right(n));
        }
        return nodeList;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        return children(n).size();
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        return null;
    }
}
