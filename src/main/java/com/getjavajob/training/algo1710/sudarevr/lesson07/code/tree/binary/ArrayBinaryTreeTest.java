package com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;

import java.util.List;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        testAddRoot();
        testAddLeft();
        testAddRight();
        testAdd();
        testLeft();
        testRight();
        testRoot();
        testRemove();
        testSet();
        testParent();
        testSize();
        testPreOrder();
        testInOrder();
        testPostOrder();
        testBreadthFirst();
    }

    private static void testAddRoot() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(10);
        assertEquals("ArrayBinaryTreeTest.testAddRoot", 10, abt.root().getElement());
    }

    private static void testAddLeft() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(10);
        abt.addLeft(abt.root(), 20);
        assertEquals("ArrayBinaryTreeTest.testAddLeft", 20, abt.left(abt.root()).getElement());
    }

    private static void testAddRight() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(10);
        abt.addRight(abt.root(), 30);
        assertEquals("ArrayBinaryTreeTest.testAddRight", 30, abt.right(abt.root()).getElement());
    }

    private static void testAdd() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        abt.add(node1, 4);
        assertEquals("ArrayBinaryTreeTest.testAdd", 4, abt.left(node1).getElement());
    }

    private static void testLeft() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);
        assertEquals("ArrayBinaryTreeTest.testLeft", 4, abt.left(node1).getElement());
    }

    private static void testRight() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);
        assertEquals("ArrayBinaryTreeTest.testRight", 5, abt.right(node1).getElement());
    }

    private static void testRoot() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);
        assertEquals("ArrayBinaryTreeTest.testRoot", 1, abt.root().getElement());
    }

    private static void testRemove() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);
        abt.remove(node5);
        String str = "1 2 3 4 5 ";

        assertEquals("ArrayBinaryTreeTest.testRemove", str, abt.toString());
    }

    private static void testSet() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        abt.set(node1, 5);
        assertEquals("ArrayBinaryTreeTest.testSet", 5, abt.left(root).getElement());
    }

    private static void testParent() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        assertEquals("ArrayBinaryTreeTest.testParent", 1, abt.parent(node1).getElement());
    }

    private static void testSize() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);

        assertEquals("ArrayBinaryTreeTest.testSize", 6, abt.size());
    }

    private static void testPreOrder() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);

        String s = "";
        List<Node<Integer>> newList = (List<Node<Integer>>) abt.preOrder();
        for (Node n : newList
                ) {
            s += n.getElement() + " ";
        }
        assertEquals("ArrayBinaryTreeTest.testPreOrder", "1 2 4 5 3 6 ", s);
    }

    private static void testInOrder() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);

        String s = "";
        List<Node<Integer>> newList = (List<Node<Integer>>) abt.inOrder();
        for (Node n : newList
                ) {
            s += n.getElement() + " ";
        }
        assertEquals("ArrayBinaryTreeTest.testInOrder", "4 2 5 1 6 3 ", s);
    }

    private static void testPostOrder() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);

        String s = "";
        List<Node<Integer>> newList = (List<Node<Integer>>) abt.postOrder();
        for (Node n : newList) {
            s += n.getElement() + " ";
        }
        assertEquals("ArrayBinaryTreeTest.testPostOrder", "4 5 2 6 3 1 ", s);
    }

    private static void testBreadthFirst() {
        ArrayBinaryTree<Integer> abt = new ArrayBinaryTree<>();
        abt.addRoot(1);
        Node<Integer> root = abt.root();
        Node<Integer> node1 = abt.addLeft(root, 2);
        Node<Integer> node2 = abt.addRight(root, 3);
        Node<Integer> node3 = abt.addLeft(node1, 4);
        Node<Integer> node4 = abt.addRight(node1, 5);
        Node<Integer> node5 = abt.addLeft(node2, 6);

        String s = "";
        List<Node<Integer>> newList = (List<Node<Integer>>) abt.breadthFirst();
        for (Node n : newList
                ) {
            s += n.getElement() + " ";
        }
        assertEquals("ArrayBinaryTreeTest.testBreadthFirst", "1 2 3 4 5 6 ", s);
    }

}
