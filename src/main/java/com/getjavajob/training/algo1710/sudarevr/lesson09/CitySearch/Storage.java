package com.getjavajob.training.algo1710.sudarevr.lesson09.CitySearch;

import java.util.*;

public class Storage {
    private NavigableSet<String> storage = new TreeSet<>(new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareToIgnoreCase(o2);
        }
    });

    public void add(String city) {
        storage.add(city);
    }

    public String search(String subString) {
        SortedSet<String> sortedSet = storage.subSet(subString, true, subString + (Character.MAX_VALUE), true);
        StringBuilder searchResult = new StringBuilder();
        for (String result : sortedSet) {
            searchResult.append(result).append(" ");
        }
        return searchResult.toString();
    }
}
