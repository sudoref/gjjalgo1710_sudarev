package com.getjavajob.training.algo1710.sudarevr.lesson09.CitySearch;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class StorageTest {
    public static void main(String[] args) {
        testStorage();
    }

    private static void testStorage() {
        Storage stor = new Storage();
        stor.add("Moscow");
        stor.add("Mogilev");
        stor.add("Saint-Petersburg");
        stor.add("Kazan");

        assertEquals("testStorage", "Mogilev Moscow", stor.search("mo"));
    }
}
