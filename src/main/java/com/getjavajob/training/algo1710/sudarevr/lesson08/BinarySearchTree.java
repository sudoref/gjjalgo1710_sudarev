package com.getjavajob.training.algo1710.sudarevr.lesson08;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;
import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {

    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (val1 instanceof Comparable) {
            return ((Comparable) val1).compareTo(val2);
        } else if (comparator != null) {
            return comparator.compare(val1, val2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {

        if (compare(n.getElement(), val) == 0) {
            return n;
        }
        if (compare(n.getElement(), val) > 0) {
            return treeSearch(left(n), val);
        } else {
            return treeSearch(right(n), val);
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }

}
