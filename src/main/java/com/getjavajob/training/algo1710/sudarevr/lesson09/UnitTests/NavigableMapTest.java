package com.getjavajob.training.algo1710.sudarevr.lesson09.UnitTests;

import java.util.NavigableMap;
import java.util.TreeMap;

import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class NavigableMapTest {
    public static void main(String[] args) {
        testLowerEntry();
        testLowerKey();
        testFloorEntry();
        testFloorKey();
        testCeilingEntry();
        testCeilingKey();
        testHigherEntry();
        testHigherKey();
        testFirstEntry();
        testLastEntry();
        testPollFirstEntry();
        testPollLastEntry();
        testDescendingMap();
        testNavigableKeySet();
        testDescendingKeySet();
        testSubMap();
        testHeadMap();
        testTailMap();
    }

    private static void testTailMap() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testTailMap", "{3=three, 4=four, 5=five}", navigableMap.tailMap(2, false).toString());
    }

    private static void testHeadMap() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testHeadMap", "{0=zero, 1=one, 2=two, 3=three}", navigableMap.headMap(4, false).toString());
    }

    private static void testSubMap() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testSubMap", "{3=three, 4=four, 5=five}", navigableMap.subMap(2, false, 5, true).toString());
    }

    private static void testDescendingKeySet() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testDescendingKeySet", "[5, 4, 3, 2, 1, 0]", navigableMap.descendingKeySet().toString());
    }

    private static void testNavigableKeySet() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testNavigableKeySet", "[0, 1, 2, 3, 4, 5]", navigableMap.navigableKeySet().toString());
    }

    private static void testDescendingMap() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testDescendingMap", "{5=five, 4=four, 3=three, 2=two, 1=one, 0=zero}", navigableMap.descendingMap().toString());
    }

    private static void testPollLastEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");
        navigableMap.pollLastEntry();

        assertEquals("testPollLastEntry", "{0=zero, 1=one, 2=two, 3=three, 4=four}", navigableMap.toString());
    }

    private static void testPollFirstEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");
        navigableMap.pollFirstEntry();

        assertEquals("testPollFirstEntry", "{1=one, 2=two, 3=three, 4=four, 5=five}", navigableMap.toString());
    }

    private static void testLastEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testLastEntry", "5=five", navigableMap.lastEntry().toString());
    }

    private static void testFirstEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");

        assertEquals("testFirstEntry", "0=zero", navigableMap.firstEntry().toString());
    }

    private static void testHigherKey() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(6, "six");
        navigableMap.put(7, "seven");
        navigableMap.put(8, "eight");
        navigableMap.put(9, "nine");

        assertEquals("testHigherKey", 6, navigableMap.higherKey(0));
    }

    private static void testHigherEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(6, "six");
        navigableMap.put(7, "seven");
        navigableMap.put(8, "eight");
        navigableMap.put(9, "nine");

        assertEquals("testHigherEntry", "9=nine", navigableMap.higherEntry(8).toString());
    }

    private static void testCeilingKey() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(6, "six");
        navigableMap.put(7, "seven");
        navigableMap.put(8, "eight");
        navigableMap.put(9, "nine");

        assertEquals("testCeilingKey", 6, navigableMap.ceilingKey(0));
    }

    private static void testCeilingEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(6, "six");
        navigableMap.put(7, "seven");
        navigableMap.put(8, "eight");
        navigableMap.put(9, "nine");

        assertEquals("testCeilingEntry", "6=six", navigableMap.ceilingEntry(2).toString());
    }

    private static void testFloorKey() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");

        assertEquals("testFloorKey", 2, navigableMap.floorKey(2));
    }

    private static void testFloorEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");

        assertEquals("testFloorEntry", "1=one", navigableMap.floorEntry(1).toString());
    }

    private static void testLowerKey() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");
        navigableMap.put(6, "six");
        navigableMap.put(7, "seven");
        navigableMap.put(8, "eight");
        navigableMap.put(9, "nine");

        assertEquals("testLowerKey", 2, navigableMap.lowerKey(3));
    }

    private static void testLowerEntry() {
        NavigableMap<Integer, String> navigableMap = new TreeMap<>();
        navigableMap.put(0, "zero");
        navigableMap.put(1, "one");
        navigableMap.put(2, "two");
        navigableMap.put(3, "three");
        navigableMap.put(4, "four");
        navigableMap.put(5, "five");
        navigableMap.put(6, "six");
        navigableMap.put(7, "seven");
        navigableMap.put(8, "eight");
        navigableMap.put(9, "nine");

        assertEquals("testLoserEntry", "2=two", navigableMap.lowerEntry(3).toString());
    }
}
