package com.getjavajob.training.algo1710.sudarevr.lesson08.balanced;

import com.getjavajob.training.algo1710.sudarevr.lesson07.code.tree.Node;
import com.getjavajob.training.algo1710.sudarevr.lesson08.BinarySearchTree;

import java.util.Stack;

public class BalanceableTree<E> extends BinarySearchTree<E> {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (makeLeftChild) {
            changeGrandParent(parent, child);
            if (child.getRight() != null) {
                child.getRight().setParent(parent);
            }
            parent.setParent(child);
            parent.setLeft(child.getRight());
            child.setRight(parent);
        } else {
            changeGrandParent(parent, child);
            if (child.getLeft() != null) {
                child.getLeft().setParent(parent);
            }
            parent.setParent(child);
            parent.setRight(child.getLeft());
            child.setLeft(parent);
        }
    }

    private void changeGrandParent(NodeImpl<E> parent, NodeImpl<E> child) {
        if (parent == root()) {
            child.setParent(null);
            addRoot(child);
        } else {
            if (parent.getParent().getLeft() == parent) {
                parent.getParent().setLeft(child);
                child.setParent(parent.getParent());
            } else {
                parent.getParent().setRight(child);
                child.setParent(parent.getParent());
            }
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        boolean makeLeftChild = compare(n.getElement(), parent(n).getElement()) < 0;
        relink(validate(parent(n)), validate(n), makeLeftChild);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> parent = validate(parent(n));
        if (parent.getLeft() != null) {
            boolean makeLeftChild = compare(parent.getElement(), parent(parent).getElement()) < 0;
            relink(parent.getParent(), parent, makeLeftChild);
            return parent;
        } else {
            rotate(n);
            boolean makeLeftChild = compare(parent.getParent().getElement(), validate(n).getParent().getElement()) < 0;
            relink(validate(n).getParent(), parent.getParent(), makeLeftChild);
            return n;
        }
    }

    public void drawTheTree(Node<E> root) {
        Stack<NodeImpl<E>> globalStack = new Stack<>();
        globalStack.push(validate(root));
        int emptySpace = 32;
        boolean isRowEmpty = false;
        while (!isRowEmpty) {
            Stack<NodeImpl<E>> localStack = new Stack<>();
            isRowEmpty = true;
            for (int j = 0; j < emptySpace; j++) {
                System.out.print(' ');
            }
            while (!globalStack.isEmpty()) {
                NodeImpl<E> currentNode = globalStack.pop();
                if (currentNode != null) {
                    if (currentNode.getBlack()) {
                        System.out.print("(" + currentNode.getElement() + ")");
                    } else {
                        System.out.print(ANSI_RED + "(" + currentNode.getElement() + ")" + ANSI_RESET);
                    }
                    localStack.push(currentNode.getLeft());
                    localStack.push(currentNode.getRight());
                    if (currentNode.getLeft() != null || currentNode.getRight() != null) {
                        isRowEmpty = false;
                    }
                }
                for (int j = 0; j < emptySpace * 2 - 2; j++) {
                    System.out.print(' ');
                }
            }
            System.out.println();
            emptySpace /= 2;
            while (!localStack.isEmpty()) {
                globalStack.push(localStack.pop());
            }
        }
    }

    @Override
    public String toString() {
        NodeImpl<E> node = validate(root());
        return toString(node);
    }

    private String toString(NodeImpl<E> node) {
        StringBuilder result = new StringBuilder();
        if (node != null) {
            result
                    .append("(")
                    .append(node.getElement())
                    .append(toString(node.getLeft()))
                    .append(toString(node.getRight()))
                    .append(")");
        }
        return result.toString();
    }
}
