package com.getjavajob.training.algo1710.sudarevr.lesson04;

import com.getjavajob.training.algo1710.util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListVSArrayList {

    public static void main(String[] args) {
        testAddBeginArrayList();
        testAddBeginLinkedList();
        testAddMiddleArrayList();
        testAddMiddleLinkedList();
        testAddEndArrayList();
        testAddEndLinkedList();

        testRemoveBeginArrayList();
        testRemoveBeginLinkedList();
        testRemoveMiddleArrayList();
        testRemoveMiddleLinkedList();
        testRemoveEndArrayList();
        testRemoveEndLinkedList();
    }

    private static void testAddBeginArrayList() {
        System.out.println("-------- Addition to the beginning --------");
        StopWatch watch = new StopWatch();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < 5000000; i++) {
            array.add(i);
        }
        watch.start();
        for (int i = 0; i < 1000; i++) {
            array.add(0, i);
        }
        System.out.println("ArrayList.add(Begin) : " + watch.getElapsedTime() + " ms");
    }

    private static void testAddBeginLinkedList() {
        StopWatch watch = new StopWatch();
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 5000000; i++) {
            list.add(i);
        }
        watch.start();
        for (int i = 0; i < 1000; i++) {
            list.add(0, i);
        }
        System.out.println("LinkedList.add(Begin)" + watch.getElapsedTime() + " ms\n");
    }

    private static void testAddMiddleArrayList() {
        System.out.println("-------- Addition to the middle --------");
        StopWatch watch = new StopWatch();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            array.add(i);
        }
        watch.start();
        for (int i = 0; i < 1000; i++) {
            array.add(5000000, i);
        }
        System.out.println("ArrayList.add(Middle)  " + watch.getElapsedTime() + " ms");
    }

    private static void testAddMiddleLinkedList() {
        StopWatch watch = new StopWatch();
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }
        watch.start();
        for (int i = 0; i < 1000; i++) {
            list.add(5000000, i);
        }
        System.out.println("LinkedList.add(middle) " + watch.getElapsedTime() + " ms");
    }

    private static void testAddEndArrayList() {
        System.out.println("-------- Addition to the end --------");
        StopWatch watch = new StopWatch();
        List<Integer> array = new ArrayList<>();
        watch.start();
        for (int i = 0; i < 10000000; i++) {
            array.add(i);
        }
        System.out.println("ArrayList.add(end) " + watch.getElapsedTime());
    }

    private static void testAddEndLinkedList() {

        StopWatch watch = new StopWatch();
        List<Integer> list = new LinkedList<>();
        watch.start();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }
        System.out.println("LinkedList.add(end) " + watch.getElapsedTime());
    }

    private static void testRemoveBeginArrayList() {
        System.out.println("------- Removing from begin --------");
        StopWatch watch = new StopWatch();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < 300000; i++) {
            array.add(i);
        }
        watch.start();
        for (int i = 0; i < 100000; i++) {
            array.remove(0);
        }
        System.out.println("ArrayList.remove(begin) " + watch.getElapsedTime() + " ms");
    }

    private static void testRemoveBeginLinkedList() {
        StopWatch watch = new StopWatch();
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 300000; i++) {
            list.add(i);
        }
        watch.start();
        for (int i = 0; i < 100000; i++) {
            list.remove(0);
        }
        System.out.println("LinkedList.remove(begin) " + watch.getElapsedTime() + " ms\n");
    }

    private static void testRemoveMiddleArrayList() {
        System.out.println("-------- Removing from mid -----------");
        StopWatch watch = new StopWatch();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < 300000; i++) {
            array.add(i);
        }
        watch.start();
        for (int i = 0; i < 100000; i++) {
            array.remove(50000);
        }
        System.out.println("ArrayList.remove(mid) " + watch.getElapsedTime() + " ms");
    }

    private static void testRemoveMiddleLinkedList() {
        StopWatch watch = new StopWatch();
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 1000000 * 3; i++) {
            list.add(i);
        }
        watch.start();
        for (int i = 0; i < 1000000; i++) {
            list.remove(50000);
        }
        System.out.println("LinkedList.remove(mid)" + watch.getElapsedTime() + " ms\n");
    }

    private static void testRemoveEndArrayList() {
        System.out.println("--------- Removing from end -----------");
        StopWatch watch = new StopWatch();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            array.add(i);
        }
        watch.start();
        for (int i = 0; i < 5000000; i++) {
            array.remove(array.size() - 1);
        }
        System.out.println("ArrayList.remove(end) " + watch.getElapsedTime() + " ms");
    }

    private static void testRemoveEndLinkedList() {
        StopWatch watch = new StopWatch();
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }
        watch.start();
        for (int i = 0; i < 5000000; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("LinkedList.remove(end) " + watch.getElapsedTime() + " ms");
    }
}

