package com.getjavajob.training.algo1710.sudarevr.lesson10;

import static com.getjavajob.training.algo1710.sudarevr.lesson10.BubbleSort.sort;
import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class BubbleSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        int[] array = {5, 7, 2, 1, 8};
        int[] sortedArray = {1, 2, 5, 7, 8};

        assertEquals("testSort", sortedArray, sort(array));
    }
}
