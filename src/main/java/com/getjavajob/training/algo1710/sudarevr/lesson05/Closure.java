package com.getjavajob.training.algo1710.sudarevr.lesson05;

public interface Closure<T> {

    void execute(T var1);
}
