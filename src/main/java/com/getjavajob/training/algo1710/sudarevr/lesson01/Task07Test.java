package com.getjavajob.training.algo1710.sudarevr.lesson01;

import static com.getjavajob.training.algo1710.sudarevr.lesson01.Task07.arithmeticSolution1;
import static com.getjavajob.training.algo1710.util.Assert.assertEquals;

public class Task07Test {
    public static void main(String[] args) {
        testArithmeticSolution1();
        testArithmeticSolution2();
        testBitwiseSolution1();
        testBitwiseSolution2();
    }

    private static void testArithmeticSolution1() {
        assertEquals("Task07.testArithmeticSolution1", "x = 20, y = 10", arithmeticSolution1(10, 20));
    }

    private static void testArithmeticSolution2() {
        assertEquals("Task07.testArithmeticSolution2", "x = 20, y = 10", arithmeticSolution1(10, 20));
    }

    private static void testBitwiseSolution1() {
        assertEquals("Task07.testBitwiseSolution1", "x = 20, y = 10", arithmeticSolution1(10, 20));
    }

    private static void testBitwiseSolution2() {
        assertEquals("Task07.testBitwiseSolution2", "x = 20, y = 10", arithmeticSolution1(10, 20));
    }
}
