package com.getjavajob.training.algo1710.sudarevr.lesson05;

import java.util.*;
import java.util.function.Predicate;

public class CollectionUtils {

    public static <T> boolean filter(Iterable<T> collection, Predicate<T> predicate) {
        boolean result = false;
        if (collection!=null && predicate!=null ) {
            Iterator<T> it = collection.iterator();
            while (it.hasNext()) {
                if (!predicate.test(it.next())) {
                    it.remove();
                    result = true;
                }
            }
        }
        return result;
    }

    public static <I, O, R extends Collection<? super O>> R collect(Iterable<I> inputCollection, Transformer<? super I, ? extends O> transformer, R outputCollection) {
        if (inputCollection != null && transformer != null) {
            for (I item : inputCollection) {
                O value = transformer.transform((I) item);
                outputCollection.add(value);
            }
        }
        return outputCollection;
    }

    public static <I, O> void transform(Iterable<I> inputCollection, Transformer<? super I, ? extends O> transformer) {
        if (inputCollection != null && transformer != null && inputCollection instanceof List) {
            List<I> resultCollection = (List<I>) inputCollection;
            ListIterator<I> it = resultCollection.listIterator();

            while (it.hasNext()) {
                it.set((I) transformer.transform(it.next()));
            }
        }
    }

    public static <T, C extends Closure<? super T>> void forAllDo(Iterable<T> collection, C closure) {
        if (closure == null) {
            throw new NullPointerException("Closure must not be null");
        } else {
            if (collection != null) {
                for (T element : collection) {
                    closure.execute(element);
                }
            }
        }
    }

    public static <T> Collection<T> unmodifiableCollection(Collection<? extends T> c) {
        return new UnmodifiableCollection<>(c);
    }

    static class UnmodifiableCollection<E> extends AbstractCollection<E> {

        Collection<? extends E> c;

        UnmodifiableCollection(Collection<? extends E> c) {
            if (c == null) {
                throw new NullPointerException();
            }
            this.c = c;
        }

        public int size() {
            return c.size();
        }

        public boolean isEmpty() {
            return c.isEmpty();
        }

        public boolean contains(Object o) {
            return c.contains(o);
        }

        public Object[] toArray() {
            return c.toArray();
        }

        public String toString() {
            return c.toString();
        }

        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> i = c.iterator();

                public boolean hasNext() {
                    return i.hasNext();
                }

                public E next() {
                    return i.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }

            };
        }

        @Override
        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        public boolean containsAll(Collection<?> coll) {
            return c.containsAll(coll);
        }

        public boolean addAll(Collection<? extends E> coll) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> coll) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> coll) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }
    }
}
