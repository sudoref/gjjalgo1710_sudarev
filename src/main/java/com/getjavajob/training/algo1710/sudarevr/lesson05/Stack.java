package com.getjavajob.training.algo1710.sudarevr.lesson05;

public interface Stack<E> {
    void push(E e);

    E pop();

    E peek();
}

