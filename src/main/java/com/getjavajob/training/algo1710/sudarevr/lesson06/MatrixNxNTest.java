package com.getjavajob.training.algo1710.sudarevr.lesson06;

import com.getjavajob.training.algo1710.util.Assert;

public class MatrixNxNTest {
    public static void main(String[] args) {
        oneMillionTest();
    }

    private static void oneMillionTest() {
        int counter = 0;
        MatrixNxN<Integer> matrixNxN = new MatrixNxN<>();
        for (int i = 0; i < 1000000; i++) {
            matrixNxN.set(i, 1000000, i);
        }

        for (int i = 1000000; i > 0; i--) {
            matrixNxN.get(i, 1000000);
            counter++;
        }

        Assert.assertEquals("oneMillionTest()", counter, matrixNxN.size());
    }
}
